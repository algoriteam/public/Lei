// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.IO;

 public class KinectModule : ModuleRules
 {
     private string ModulePath
     {
         get { return ModuleDirectory; }
     }

     private string KinectPath
     {
         get { return Path.GetFullPath( Path.Combine( ModulePath, "../../Kinect/" ) ); }
     }

     public KinectModule(TargetInfo Target)
     {
         Type = ModuleType.External;

         // COMMON
         string common_inc = Path.Combine( KinectPath, "common_v1.8/inc" );
         string common_lib_86 = Path.Combine( KinectPath, "common_v1.8/lib/x86" );
         string common_lib_64 = Path.Combine( KinectPath, "common_v1.8/lib/amd64" );

         string common_dll = Path.Combine( KinectPath, "common_v1.8/Assemblies/Microsoft.Kinect.dll" );

				 PublicIncludePaths.Add(common_inc);

         if ((Target.Platform == UnrealTargetPlatform.Win64))
            PublicLibraryPaths.Add(common_lib_64);
				 else PublicLibraryPaths.Add(common_lib_86);

         PublicDelayLoadDLLs.Add(common_dll);
         PublicAdditionalLibraries.Add( "Kinect10.lib" );

         // DEVELOPER
         string dev_inc = Path.Combine( KinectPath, "dev_v1.8/inc" );
         string dev_lib_86 = Path.Combine( KinectPath, "dev_v1.8/lib/x86" );
         string dev_lib_64 = Path.Combine( KinectPath, "dev_v1.8/lib/amd64" );

         string dev_dll_1 = Path.Combine( KinectPath, "dev_v1.8/Assemblies/Microsoft.Kinect.Toolkit.BackgroundRemoval.dll" );
         string dev_dll_2 = Path.Combine( KinectPath, "dev_v1.8/Assemblies/Microsoft.Kinect.Toolkit.Controls.dll" );
         string dev_dll_3 = Path.Combine( KinectPath, "dev_v1.8/Assemblies/Microsoft.Kinect.Toolkit.dll" );
         string dev_dll_4 = Path.Combine( KinectPath, "dev_v1.8/Assemblies/Microsoft.Kinect.Toolkit.Fusion.dll" );
         string dev_dll_5 = Path.Combine( KinectPath, "dev_v1.8/Assemblies/Microsoft.Kinect.Toolkit.Interaction.dll" );

				 PublicIncludePaths.Add(dev_inc);

         if ((Target.Platform == UnrealTargetPlatform.Win64))
            PublicLibraryPaths.Add(dev_lib_64);
				 else PublicLibraryPaths.Add(dev_lib_86);

         PublicDelayLoadDLLs.Add(dev_dll_1);
         PublicDelayLoadDLLs.Add(dev_dll_2);
         PublicDelayLoadDLLs.Add(dev_dll_3);
         PublicDelayLoadDLLs.Add(dev_dll_4);
         PublicDelayLoadDLLs.Add(dev_dll_5);

         PublicAdditionalLibraries.Add( "FaceTrackLib.lib" );
         PublicAdditionalLibraries.Add( "KinectBackgroundRemoval180_64.lib" );
         PublicAdditionalLibraries.Add( "KinectFusion180_64.lib" );
         PublicAdditionalLibraries.Add( "KinectInteraction180_64.lib" );
     }
 }
