// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

/**
 * 
 */
class IORD_API Voxel
{
	// Position
	int i;
	int j;
	int k;

	// Voxel represented by a cube
	float size;

	// Useful for occupancy and density grids
	TArray<FVector> points;

public:
	Voxel();
	Voxel(int a, int b, int c);
	Voxel(int a, int b, int c, float s);
	~Voxel();
	int getI();
	int getJ();
	int getK();

	FVector getCentroid();
	float getSize();

	TArray<FVector> getPoints();
	void clearPoints();
	int getNumPoints();

	void addPoint(FVector point);
	bool isOccupied();

	bool operator==(const Voxel &v) const
	{
		return i == v.i && j == v.j && k == v.k;
	}
};
