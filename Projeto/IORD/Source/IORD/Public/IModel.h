// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CloudPoint.h"
#include "VoxelGrid.h"
#include "Generator.h"
#include "Matrix33Double.h"
#include "IPlane.h"

#include "GameFramework/Actor.h"
#include "IModel.generated.h"

UCLASS()
class IORD_API AIModel : public AActor
{
	GENERATED_BODY()

		// Voxels Grid
		VoxelGrid grid;

		// Cloud Point
		CloudPoint origin;
		CloudPoint cloud;

		// Transformations auxiliaries
		float unit_x, unit_y, unit_z;

public:
	// Sets default values for this actor's properties
	AIModel();

	// Setup
	UFUNCTION(BlueprintCallable)
	void clear();
	UFUNCTION(BlueprintCallable)
	void setup(int form, FVector position, float a, float b, int density, float gridSize, float voxelSize);
	UFUNCTION(BlueprintCallable)
	void loadModelCloud(FString filename, float gridSize, float voxelSize);

	// Transformations
	UFUNCTION(BlueprintCallable)
	void reset();
	UFUNCTION(BlueprintCallable)
	void scale(float x, float y, float z, bool changeUnit);
	UFUNCTION(BlueprintCallable)
	void translate(float x, float y, float z);
	UFUNCTION(BlueprintCallable)
	void rotateX(float angle);
	UFUNCTION(BlueprintCallable)
	void rotateY(float angle);
	UFUNCTION(BlueprintCallable)
	void rotateZ(float angle);

	// DRAW
	UFUNCTION(BlueprintCallable)
	void drawGrid();
	UFUNCTION(BlueprintCallable)
	void drawPoints(FColor color);
	UFUNCTION(BlueprintCallable)
	void drawVoxels(bool goForOccupied);
	// TEST GRID
	UFUNCTION(BlueprintCallable)
	void testGrid();
	// TEST NOISE
	UFUNCTION(BlueprintCallable)
    void noise(float level);
	UFUNCTION(BlueprintCallable)
	void testNoise(float level);
	// SAVE MODEL
	UFUNCTION(BlueprintCallable)
	void saveModel(FString filename, FString description, int type, int rotationInvariant, bool mirrorRebuild, bool treatData); // Type = OCCUPANCY | DENSITY | NORMALS, RotationInvariant = NORMALS | DENSITY
	// CREATE TRAINING SET
	UFUNCTION(BlueprintCallable)
	void createSet(FString filename, int label, FString description, int mode, int type, int numModels, float maxNoise); // Mode = SIMPLE | ADVANCED | CUSTOM, Type = OCCUPANCY | DENSITY | NORMALS

	// ITERATIVE CLOSEST POINT
	/*UFUNCTION(BlueprintCallable)
	void reconstruct(int maxIterationsPerModel, float minError, int numberPoints, TArray<AIModel> aux_models);*/

	// CALCULATE NORMALS
	UFUNCTION(BlueprintCallable)
	void showNormals(float gridSize, float voxelSize, int numIterations);

	// FIX POSITION
	UFUNCTION(BlueprintCallable)
	void normalize();
	UFUNCTION(BlueprintCallable)
	void center();
	UFUNCTION(BlueprintCallable)
	void rotateWithOlls(int ollsNumIterations, bool debug, FColor color);
	UFUNCTION(BlueprintCallable)
	void rotateWithNormals(float gridSize, float voxelSize, float tolerance, int normalsNumIterations, bool debug, FColor color);
	UFUNCTION(BlueprintCallable)
	void rotateWithDensity(float gridSize, float voxelSize, float tolerance, bool debug, FColor color);


	UFUNCTION(BlueprintCallable)
	void showRotationInvariantOLLS(int numIterations, FColor color);
	UFUNCTION(BlueprintCallable)
	void showRotationInvariantNormals(float gridSize, float voxelSize, float tolerance, int numIterations, FColor color);
	UFUNCTION(BlueprintCallable)
	void showRotationInvariantDensity(float gridSize, float voxelSize, float tolerance, FColor color);

	UFUNCTION(BlueprintCallable)
	void mirrorRebuild();

private:
	void reset(CloudPoint cloud);
	void rebuildVoxels(CloudPoint c);

	TArray<FVector> translate(TArray<FVector> a, float x, float y, float z);

	int calculateLabel(FString path, FString description);
	void writeModelLeNet(FString JsonStr, FString description, TArray<int> list);
	void writeSetLeNet(FString JsonStr, int label, FString description, TArray<TArray<int>> dimension);

	void writeModelCloud(FString path, FString description);

	void writeModelLeVector(FString path, FString description, TArray<IPlane> list);
	void writeSetLeVector(FString JsonStr, int label, FString description, TArray<TArray<IPlane>> dimension);

	FVector getOLLSDominantVector(int numIterations, bool draw, FColor color);
	FVector getNormalsDominantVector(int gridSize, float voxelSize, float tolerance, int numIterations, bool draw, FColor color);
	FVector getDensityDominantVector(int gridSize, float voxelSize, float tolerance, bool draw, FColor color);
	void alignVectors(FVector a, FVector b);

	IPlane FindOLLSPlane(int numIterations);

	TArray<FVector> mirrorCorrection(TArray<FVector> a, TArray<FVector> b);
	bool pointsTouch(TArray<FVector> a, TArray<FVector> b);

	/*
	void transpose(float **a, float **b, int m, int n);
	void matrix_multiply(float **a, float **b, float **c, int m, int n, int o);
	float PYTHAG(float a, float b);
	int svd(float **a, int m, int n, float *w, float **v);
	float matchPercentage(CloudPoint a, CloudPoint b);
	float errorFunction(TArray<std::tuple<FVector, FVector, float>> points);
	*/

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
