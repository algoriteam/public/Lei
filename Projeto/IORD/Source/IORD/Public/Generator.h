// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CloudPoint.h"

/**
 * 
 */
class IORD_API Generator
{
public:
	Generator();
	~Generator();

	// CUBE
	CloudPoint generateCube(FVector pos, float size, int pointDensity);

	// SPHERE
	CloudPoint generateSphere(FVector pos, float radius, int pointDensity);

	// CONE
	CloudPoint generateCone(FVector pos, float radius, float height, int pointDensity);

	// CILINDER
	CloudPoint generateCylinder(FVector pos, float radius, float height, int pointDensity);

	// PYRAMID
	CloudPoint generatePyramid(FVector pos, float squareSize, float height, int pointDensity);

private:
	// CUBE
	CloudPoint generateCube_XY_Face(FVector c, float size, float pointDensity);
	CloudPoint generateCube_XZ_Face(FVector c, float size, float pointDensity);
	CloudPoint generateCube_YZ_Face(FVector c, float size, float pointDensity);

	// SPHERE
	// None

	// CONE
	CloudPoint generateCone_XY_Face(FVector c, float radius, float pointDensity);

	// CILINDER
	CloudPoint generateCylinder_XY_Face(FVector c, float radius, float pointDensity);

	// PYRAMID
	CloudPoint generatePyramid_XY_Face(FVector c, float squareSize, float pointDensity);
	CloudPoint generatePyramid_XY_Square(FVector c, float squareSize, float pointDensity);
};
