// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CloudPoint.h"
#include "Voxel.h"
#include "Matrix33.h"
#include "IPlane.h"

/**
 * 
 */
class IORD_API VoxelGrid
{
	// Grid center origin
	FVector pos;

	// List of voxels
	TArray<Voxel> voxels;

	// Grid is represented by a cube
	float size;

	// Voxel is represented by a cube
	float voxelSize;

public:
	VoxelGrid();
	VoxelGrid(FVector p, float s, float vs);
	~VoxelGrid();

	void voxelize(CloudPoint c);
	FVector getPosition();
	TArray<Voxel> getVoxels();
	Voxel getVoxel(int position);
	float getSize();
	float getVoxelSize();

	float getMaximum();

	TArray<int> getOccupancy();
	TArray<int> getDensity();
	TArray<IPlane> getNormals(int numIterations);

	float similarity(VoxelGrid v, int type);

	void clear();

private:
	void clearAllVoxels();
	void generateAllVoxels();
	void addVoxel(Voxel v);
	void removeVoxel(Voxel v);
	void addToVoxel(FVector point, int i, int j, int k);
	IPlane FindOLLSPlane(FVector position, TArray<FVector> points, int numIterations);
};
