// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

/**
 * 
 */
class IORD_API IPlane
{
	// Voxel position
	FVector position;

	// Centroid
	FVector centroid;

	// Normal
	FVector normal;

public:
	IPlane();
	IPlane(FVector p, FVector c, FVector n);
	~IPlane();

	FVector getPosition();
	FVector getCentroid();
	FVector getNormal();

	bool operator==(const IPlane &p) const
	{
		return position.Equals(p.position) && centroid.Equals(p.centroid) && normal.Equals(p.normal);
	}
};
