// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

/**
*
*/
class IORD_API Matrix33Double
{
	double mat[3][3];

public:
	Matrix33Double();
	Matrix33Double(double x1, double x2, double x3,
		           double y1, double y2, double y3,
		           double z1, double z2, double z3);
	Matrix33Double(double m[3][3]);
	~Matrix33Double();

	double* getLine(int position);
	double* getColumn(int position);

	double getDeterminant();
	Matrix33Double getInverse();

	FVector findEigenVectorAssociatedWithLargestEigenValue(int numIterations);

	Matrix33Double scale(double value);
	Matrix33Double power(int value);
	Matrix33Double multiply(Matrix33Double m);
	FVector multiply(FVector v);

	FString print();

private:
	double multiply(double* line, double* column);
	int* gaussianPivoting();
	double findLargestEntry();
};
