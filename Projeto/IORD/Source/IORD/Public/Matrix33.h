// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

/**
 * 
 */
class IORD_API Matrix33
{
	float mat[3][3];

public:
	Matrix33();
	Matrix33(float x1, float x2, float x3,
			 float y1, float y2, float y3,
			 float z1, float z2, float z3);
	Matrix33(float m[3][3]);
	~Matrix33();

	float* getLine(int position);
	float* getColumn(int position);

	float getDeterminant();
	Matrix33 getInverse();

	FVector findEigenVectorAssociatedWithLargestEigenValue(int numIterations);

	Matrix33 scale(float value);
	Matrix33 power(int value);
	Matrix33 multiply(Matrix33 m);
	FVector multiply(FVector v);

	FString print();

private:
	float multiply(float* line, float* column);
	int* gaussianPivoting();
	float findLargestEntry();
};
