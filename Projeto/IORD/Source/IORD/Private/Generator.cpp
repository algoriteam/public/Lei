// Fill out your copyright notice in the Description page of Project Settings.

#include "IORD.h"
#include "Generator.h"

#define PI 3.14159265
#define PI2 6.28318531
#define hPI 1.57079633
#define qPI 0.78539816

// ----- CONSTRUCTORS ----- //

Generator::Generator()
{
}

Generator::~Generator()
{
}

// ----- METHODS ----- //

// ----- GENERATE CUBE ----- //
CloudPoint Generator::generateCube(FVector pos, float size, int pointDensity)
{
	CloudPoint res = CloudPoint();
	float half_size = size * 0.5f;

	FVector face1 = FVector(pos.X, pos.Y + half_size, pos.Z); // FRONT
	FVector face2 = FVector(pos.X + half_size, pos.Y, pos.Z); // RIGHT
	FVector face3 = FVector(pos.X, pos.Y - half_size, pos.Z); // BACK
	FVector face4 = FVector(pos.X - half_size, pos.Y, pos.Z); // LEFT
	FVector face5 = FVector(pos.X, pos.Y, pos.Z + half_size); // UP
	FVector face6 = FVector(pos.X, pos.Y, pos.Z - half_size); // BOTTOM

	// SURFACE 1
	res.addPoints(this->generateCube_XZ_Face(face1, size, pointDensity));
	// SURFACE 2
	res.addPoints(this->generateCube_YZ_Face(face2, size, pointDensity));
	// SURFACE 3
	res.addPoints(this->generateCube_XZ_Face(face3, size, pointDensity));
	// SURFACE 4
	res.addPoints(this->generateCube_YZ_Face(face4, size, pointDensity));
	// SURFACE 5
	res.addPoints(this->generateCube_XY_Face(face5, size, pointDensity));
	// SURFACE 6
	res.addPoints(this->generateCube_XY_Face(face6, size, pointDensity));

	return res;
}

CloudPoint Generator::generateCube_XY_Face(FVector c, float size, float pointDensity)
{
	CloudPoint res = CloudPoint();
	float half_size = size * 0.5f;

	float start_x = c.X - half_size;
	float start_y = c.Y - half_size;
	float current_x;
	float current_y;

	float jump = size / pointDensity;
	float current_jump = 0.0f;

	if (pointDensity > 0) {
		for (int a = 0; a <= pointDensity; a++) {
			current_x = start_x + a * jump;
			for (int b = 0; b <= pointDensity; b++) {
				current_y = start_y + b * jump;
				res.addPoint(FVector(current_x, current_y, c.Z));
			}
		}
	}

	return res;
}

CloudPoint Generator::generateCube_XZ_Face(FVector c, float size, float pointDensity)
{
	CloudPoint res = CloudPoint();
	float half_size = size * 0.5f;

	float start_x = c.X - half_size;
	float start_z = c.Z - half_size;
	float current_x;
	float current_z;

	float jump = size / pointDensity;
	float current_jump = 0.0f;

	if (pointDensity > 0) {
		for (int a = 0; a <= pointDensity; a++) {
			current_x = start_x + a * jump;
			for (int b = 0; b <= pointDensity; b++) {
				current_z = start_z + b * jump;
				res.addPoint(FVector(current_x, c.Y, current_z));
			}
		}
	}

	return res;
}

CloudPoint Generator::generateCube_YZ_Face(FVector c, float size, float pointDensity)
{
	CloudPoint res = CloudPoint();
	float half_size = size * 0.5f;

	float start_y = c.Y - half_size;
	float start_z = c.Z - half_size;
	float current_y;
	float current_z;

	float jump = size / pointDensity;

	if (pointDensity > 0) {
		for (int a = 0; a <= pointDensity; a++) {
			current_y = start_y + a * jump;
			for (int b = 0; b <= pointDensity; b++) {
				current_z = start_z + b * jump;
				res.addPoint(FVector(c.X, current_y, current_z));
			}
		}
	}

	return res;
}

// ----- GENERATE SPHERE ----- //
CloudPoint Generator::generateSphere(FVector pos, float radius, int pointDensity) {
	CloudPoint res = CloudPoint();
	float start_alpha = 0.0f; // X
	float start_beta = 0.0f; // Y
	float current_alpha;
	float current_beta;

	float x, y, z;

	float alpha_jump = hPI / pointDensity;
	float beta_jump = hPI / pointDensity;

	if (pointDensity > 0) {
		current_beta = 0.0f;
		for (int b = 0; current_beta <= PI; b++) {
			current_beta = b * beta_jump;

			current_alpha = 0.0f;
			for (int a = 0; current_alpha <= PI2; a++) {
				current_alpha = a * alpha_jump;

				x = pos.X + radius * cosf(current_alpha) * sinf(current_beta);
				y = pos.Y + radius * sinf(current_alpha) * sinf(current_beta);
				z = pos.Z + radius * cosf(current_beta);
				res.addPoint(FVector(x, y, z));
			}
		}
	}

	return res;
}

// ----- GENERATE CONE ----- //
CloudPoint Generator::generateCone(FVector pos, float radius, float height, int pointDensity) {
	CloudPoint res = CloudPoint();

	// POINT
	float half_height = height * 0.5f;
	FVector start_point = FVector(pos.X, pos.Y - half_height, pos.Z);

	// CIRCLE
	float current_alpha;
	float alpha_jump = hPI / pointDensity;

	// HEIGHT
	float current_radius;
	float radius_jump = radius / pointDensity;

	float current_height;
	float height_jump = height / pointDensity;

	float x, y, z;

	// BASE
	CloudPoint base = generateCone_XY_Face(start_point, radius, pointDensity);
	res.addPoints(base);

	// CORPO
	if (pointDensity > 0) {
		current_radius = radius;
		for (int a = 1; current_radius > 0.0f; a++) {
			current_radius = radius - a * radius_jump;
			current_height = a * height_jump;

			current_alpha = 0.0f;
			for (int b = 0; current_alpha <= PI2; b++) {
				current_alpha = b * alpha_jump;

				x = start_point.X + current_radius * cosf(current_alpha);
				y = start_point.Z + current_radius * sinf(current_alpha);
				z = start_point.Y + current_height;
				res.addPoint(FVector(x, y, z));
			}
		}
	}

	return res;
}

CloudPoint Generator::generateCone_XY_Face(FVector c, float radius, float pointDensity)
{
	CloudPoint res = CloudPoint();
	// ANGLE
	float current_alpha;
	float alpha_jump = hPI / pointDensity;

	// RADIUS
	float current_radius;
	float radius_jump = radius / pointDensity;

	float x, y, z;

	if (pointDensity > 0) {
		for (int a = 0; a <= pointDensity; a++) {
			current_radius = radius - a * radius_jump;
			for (int b = 0; b < pointDensity * 4; b++) {
				current_alpha = b * alpha_jump;

				x = c.X + current_radius * cosf(current_alpha);
				y = c.Z + current_radius * sinf(current_alpha);
				z = c.Y;
				res.addPoint(FVector(x, y, z));
			}
		}
	}

	return res;
}

// ----- GENERATE CYLINDER ----- //
CloudPoint Generator::generateCylinder(FVector pos, float radius, float height, int pointDensity) {
	CloudPoint res = CloudPoint();

	// POINT
	float half_height = height * 0.5f;
	FVector ending_point = FVector(pos.X, pos.Y + half_height, pos.Z);
	FVector start_point = FVector(pos.X, pos.Y - half_height, pos.Z);

	// CIRCLE
	float current_alpha;
	float alpha_jump = hPI / pointDensity;

	// HEIGHT
	float current_height;
	float height_jump = height / pointDensity;

	float x, y, z;

	// BASE TOP
	CloudPoint top = generateCylinder_XY_Face(ending_point, radius, pointDensity);
	res.addPoints(top);

	// BASE BOTTOM
	CloudPoint bottom = generateCylinder_XY_Face(start_point, radius, pointDensity);
	res.addPoints(bottom);

	// CORPO
	if (pointDensity > 0) {
		for (int a = 1; a < pointDensity; a++) {
			current_height = a * height_jump;

			for (int b = 0; b < pointDensity * 4; b++) {
				current_alpha = b * alpha_jump;

				x = start_point.X + radius * cosf(current_alpha);
				y = start_point.Z + radius * sinf(current_alpha);
				z = start_point.Y + current_height;
				res.addPoint(FVector(x, y, z));
			}
		}
	}

	return res;
}

CloudPoint Generator::generateCylinder_XY_Face(FVector c, float radius, float pointDensity)
{
	CloudPoint res = CloudPoint();
	// ANGLE
	float current_alpha;
	float alpha_jump = hPI / pointDensity;

	// RADIUS
	float current_radius;
	float radius_jump = radius / pointDensity;

	float x, y, z;

	if (pointDensity > 0) {
		for (int a = 0; a <= pointDensity; a++) {
			current_radius = radius - a * radius_jump;
			for (int b = 0; b < pointDensity * 4; b++) {
				current_alpha = b * alpha_jump;

				x = c.X + current_radius * cosf(current_alpha);
				y = c.Z + current_radius * sinf(current_alpha);
				z = c.Y;
				res.addPoint(FVector(x, y, z));
			}
		}
	}

	return res;
}

// ----- GENERATE PYRAMID ----- //
CloudPoint Generator::generatePyramid(FVector pos, float squareSize, float height, int pointDensity) {
	CloudPoint res = CloudPoint();

	// POINT
	float half_height = height * 0.5f;
	FVector start_point = FVector(pos.X, pos.Y, pos.Z - half_height);

	// HEIGHT
	float current_square;
	float square_jump = squareSize / pointDensity;

	float current_height;
	float height_jump = height / pointDensity;

	// BASE
	CloudPoint base = generatePyramid_XY_Face(start_point, squareSize, pointDensity);
	res.addPoints(base);

	// CORPO
	if (pointDensity > 0) {
		for (int a = 1; a <= pointDensity; a++) {
			current_height = a * height_jump;
			current_square = squareSize - a * square_jump;

			CloudPoint square = generatePyramid_XY_Square(FVector(start_point.X, start_point.Y, start_point.Z + current_height), current_square, pointDensity);
			res.addPoints(square);
		}
	}

	return res;
}

CloudPoint Generator::generatePyramid_XY_Square(FVector c, float size, float pointDensity)
{
	CloudPoint res = CloudPoint();
	float half_size = size * 0.5f;

	float start_x = c.X - half_size;
	float start_y = c.Y - half_size;
	float ending_y = c.Y + half_size;
	float current_x;
	float current_y;

	float jump = size / pointDensity;
	float current_jump = 0.0f;

	if (pointDensity > 0) {
		for (int a = 0; a <= pointDensity; a++) {
			current_x = start_x + a * jump;

			if (a == 0 || a == pointDensity) {
				for (int b = 0; b <= pointDensity; b++) {
					current_y = start_y + b * jump;
					res.addPoint(FVector(current_x, current_y, c.Z));
				}
			}
			else {
				res.addPoint(FVector(current_x, start_y, c.Z));
				res.addPoint(FVector(current_x, ending_y, c.Z));
			}
		}
	}

	return res;
}

CloudPoint Generator::generatePyramid_XY_Face(FVector c, float size, float pointDensity)
{
	CloudPoint res = CloudPoint();
	float half_size = size * 0.5f;

	float start_x = c.X - half_size;
	float start_y = c.Y - half_size;
	float current_x;
	float current_y;

	float jump = size / pointDensity;
	float current_jump = 0.0f;

	if (pointDensity > 0) {
		for (int a = 0; a <= pointDensity; a++) {
			current_x = start_x + a * jump;
			for (int b = 0; b <= pointDensity; b++) {
				current_y = start_y + b * jump;
				res.addPoint(FVector(current_x, current_y, c.Z));
			}
		}
	}

	return res;
}
