// Fill out your copyright notice in the Description page of Project Settings.

#include "IORD.h"
#include "IModel.h"

#include <string>

#include <iostream>
#include <fstream>

// ------------------------ //
// ----- CONSTRUCTORS ----- //
// ------------------------ //

// Sets default values
AIModel::AIModel()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	this->unit_x = 1.0f;
	this->unit_y = 1.0f;
	this->unit_z = 1.0f;
}

// Called when the platform starts or when the object is spawned
void AIModel::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AIModel::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// --------------------------- //
// ----- TRANSFORMATIONS ----- //
// --------------------------- //
void AIModel::reset() {
	// Reset unit scale
	this->unit_x = 1.0f;
	this->unit_y = 1.0f;
	this->unit_z = 1.0f;

	// Reset origin cloudpoint
	this->cloud = CloudPoint();

	// Add new points
	this->cloud.addPoints(this->origin);

	// Rebuild voxelgrid
	this->rebuildVoxels(this->cloud);
}

void AIModel::reset(CloudPoint c) {
	// Reset origin cloudpoint
	this->cloud = CloudPoint();

	// Add new points
	this->cloud.addPoints(c);

	// Rebuild voxelgrid
	this->rebuildVoxels(this->cloud);
}

void AIModel::scale(float x, float y, float z, bool changeUnit) {
	CloudPoint res = CloudPoint();

	if (changeUnit) {
		this->unit_x = x;
		this->unit_y = y;
		this->unit_z = z;
	}

	for (FVector v : this->cloud.getCloud()) {
		res.addPoint(FVector(v.X * x, v.Y * y, v.Z * z));
	}

	this->reset(res);
}

void AIModel::translate(float x, float y, float z) {
	CloudPoint res = CloudPoint();

	for (FVector v : this->cloud.getCloud()) {
		res.addPoint(FVector(v.X + x * this->unit_x, v.Y + y * this->unit_y, v.Z + z * this->unit_z));
	}

	this->reset(res);
}

TArray<FVector> AIModel::translate(TArray<FVector> a, float x, float y, float z) {
	TArray<FVector> res;

	for (FVector v : a) {
		res.AddUnique(FVector(v.X + x, v.Y + y, v.Z + z));
	}

	return res;
}

void AIModel::rotateX(float angle) {
	// x' = x
	// y' = y * cos(v) - z * sin(v)
	// z' = y * sin(v) + z * cos(v)

	FVector xAxis = FVector(1.0f, 0.0f, 0.0f);
	CloudPoint res = CloudPoint();

	float radian = FMath::DegreesToRadians(angle);

	for (FVector v : this->cloud.getCloud()) {
		res.addPoint(v.RotateAngleAxis(angle, xAxis));
	}

	this->reset(res);
}

void AIModel::rotateY(float angle) {
	// x' = x * cos(v) + z * sin(v)
	// y' = y
	// z' = -x * sin(v) + z * cos(v)

	FVector yAxis = FVector(0.0f, 1.0f, 0.0f);
	CloudPoint res = CloudPoint();

	float radian = FMath::DegreesToRadians(angle);

	for (FVector v : this->cloud.getCloud()) {
		res.addPoint(v.RotateAngleAxis(angle, yAxis));
	}

	this->reset(res);
}

void AIModel::rotateZ(float angle) {
	// x' = x * cos(v) - y * sin(v)
    // y' = x * sin(v) + y * cos(v)
    // z' = z

	FVector zAxis = FVector(0.0f, 0.0f, 1.0f);
	CloudPoint res = CloudPoint();

	float radian = FMath::DegreesToRadians(angle);

	for (FVector v : this->cloud.getCloud()) {
		res.addPoint(v.RotateAngleAxis(angle, zAxis));
	}

	this->reset(res);
}

// --------------------------------------------------- //
// ----- CORRECT SCALE, TRANSLATION AND ROTATION ----- //
// --------------------------------------------------- //

// ----- SCALE ----- //
void AIModel::normalize() {
	float max = this->cloud.getMaximum();
	float limit = this->grid.getMaximum();
	float diff = limit / max;

	// UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Max cloud value is %.2f ..."), max);
	// UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Limit grid value is %.2f ..."), limit);
	// UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Grid ratio to model = %.6fx ..."), diff);

	// UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Normalizing model ..."));
	this->scale(diff, diff, diff, false);
}

// ----- TRANSLATION ----- //
void AIModel::center() {
	FVector model_center = this->cloud.getCentroid();
	FVector grid_center = this->grid.getPosition();
	FVector move = grid_center - model_center;

	// UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Model center is ( %.2f, %.2f, %.2f ) ..."), model_center.X, model_center.Y, model_center.Z);
	// UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Grid center is ( %.2f, %.2f, %.2f ) ..."), grid_center.X, grid_center.Y, grid_center.Z);

	// UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Translating model ..."));
	this->translate(move.X, move.Y, move.Z);
}

// ----- ROTATION ----- //
void AIModel::rotateWithOlls(int ollsNumIterations, bool debug, FColor color) {
	FVector ollsDominant;
	FVector xozPlane;

	// Draw debug points
	if (debug) this->drawPoints(color);

	ollsDominant = this->getOLLSDominantVector(ollsNumIterations, debug, color);
	xozPlane = FVector(FMath::Abs(ollsDominant.X), 0.0f, FMath::Abs(ollsDominant.Z));

	// ROTATE OLLS TO XoZ PLANE
	this->alignVectors(ollsDominant, xozPlane);
}

void AIModel::rotateWithNormals(float gridSize, float voxelSize, float tolerance, int normalsNumIterations, bool debug, FColor color) {
	FVector normalsDominant;
	FVector zAxis = FVector(0.0f, 0.0f, 1.0f);

	// Draw debug points
	if (debug) this->drawPoints(color);

	normalsDominant = this->getNormalsDominantVector(gridSize, voxelSize, tolerance, normalsNumIterations, debug, color);

	// ROTATE NORMAL TO Z AXIS
	this->alignVectors(normalsDominant, zAxis);
}

void AIModel::rotateWithDensity(float gridSize, float voxelSize, float tolerance, bool debug, FColor color) {
	FVector densityDominant;
	FVector zAxis = FVector(0.0f, 0.0f, 1.0f);

	// Draw debug points
	if (debug) this->drawPoints(color);

	densityDominant = this->getDensityDominantVector(gridSize, voxelSize, tolerance, debug, color);

	// ROTATE DENSITY TO Z AXIS
	this->alignVectors(densityDominant, zAxis);
}

FVector AIModel::getOLLSDominantVector(int numIterations, bool draw, FColor color) {
	FVector res = FVector(0.0f);
	FVector object_center = this->cloud.getCentroid();

	IPlane plane = FindOLLSPlane(numIterations);
	res = plane.getNormal();
	// UE_LOG(LogTemp, Warning, TEXT("[DEBUG] OLLS Dominant Vector = ( %.2f, %.2f, %.2f ) ..."), res.X, res.Y, res.Z);

	if (draw) {
		DrawDebugDirectionalArrow(
			GetWorld(),
			object_center,
			res * this->grid.getSize(),
			0.2f,
			color,
			1000,
			-1.0f,
			(uint8)'\000',
			0.1f
		);
	}

	return plane.getNormal();
}

IPlane AIModel::FindOLLSPlane(int numIterations) {
	// <Centroid, Normal>
	IPlane res;

	int count = this->cloud.numPoints();
	Matrix33Double mat;
	TArray<FVector> points = this->cloud.getCloud();

	double sumX = 0.0;
	double sumY = 0.0;
	double sumZ = 0.0;

	FVector center;
	FVector normal;
	FVector end;

	// Calculate points sum
	for (int i = 0; i < count; i++) {
		sumX += points[i].X;
		sumY += points[i].Y;
		sumZ += points[i].Z;
	}

	// Calculate points centroid
	sumX /= count;
	sumY /= count;
	sumZ /= count;
	center = FVector(sumX, sumY, sumZ);

	double sumXX = 0.0, sumXY = 0.0, sumXZ = 0.0;
	double sumYY = 0.0, sumYZ = 0.0;
	double sumZZ = 0.0;

	double diffX, diffY, diffZ;

	for (int i = 0; i < count; i++) {
		diffX = points[i].X - center.X;
		diffY = points[i].Y - center.Y;
		diffZ = points[i].Z - center.Z;

		sumXX += diffX * diffX;
		sumXY += diffX * diffY;
		sumXZ += diffX * diffZ;
		sumYY += diffY * diffY;
		sumYZ += diffY * diffZ;
		sumZZ += diffZ * diffZ;
	}

	// Prepare matrix for eigenvectors calculation
	mat = Matrix33Double(sumXX, sumXY, sumXZ,
		                 sumXY, sumYY, sumYZ,
		                 sumXZ, sumYZ, sumZZ);

	/* TEST MATRICES
	Matrix33 mat1 = Matrix33(6,1,1,
	4,-2,5,
	2,8,7);

	Matrix33 mat2 = Matrix33(1, 2, 3,
	4, 5, 6,
	7, 8, 9);

	Matrix33 scaleMatrix = mat1.scale(2.0f);
	Matrix33 multiplyMatrix = mat1.multiply(mat2);
	FVector multiplyVector = mat1.multiply(FVector(1, 2, 3));
	float determinant = mat1.getDeterminant();
	Matrix33 inverse = mat1.getInverse();

	UE_LOG(LogTemp, Warning, TEXT("[DEBUG] original: %s"), *mat1.print()); // { 6.00, 1.00, 1.00, 4.00, -2.00, 5.00, 2.00, 8.00, 7.00 }
	UE_LOG(LogTemp, Warning, TEXT("[DEBUG] determinant: %.6f"), determinant); // -306
	UE_LOG(LogTemp, Warning, TEXT("[DEBUG] inverse: %s"), *inverse.print());
	UE_LOG(LogTemp, Warning, TEXT("[DEBUG] scaleMatrix: %s"), *scaleMatrix.print()); // { 12.00, 2.00, 2.00, 8.00, -4.00, 10.00, 4.00, 16.00, 14.00 }
	UE_LOG(LogTemp, Warning, TEXT("[DEBUG] multiplyMatrix: %s"), *multiplyMatrix.print()); // { 17.00, 25.00, 33.00, 31.00, 38.00, 45.00, 83.00, 100.00, 117.00 }
	UE_LOG(LogTemp, Warning, TEXT("[DEBUG] multiplyVector: %s"), *multiplyVector.ToString()); // X=11.000 Y=15.000 Z=39.000
	*/

	// Calculate determinant
	double det = mat.getDeterminant();

	if (det < 1e-16f) {
		normal = FVector(0.0f);
	}
	else {
		Matrix33Double matInverse = mat.getInverse();
		normal = matInverse.findEigenVectorAssociatedWithLargestEigenValue(numIterations);

		// Check if vector as any value > or < 1 (float shows errors when rounding), even with normalization!
		if (FMath::Abs(normal.X) <= 1.0f && FMath::Abs(normal.Y) <= 1.0f && FMath::Abs(normal.Z) <= 1.0f) {
			end = center + normal;

			// CORRECT NORMAL ORIENTATION
			float centroid_to_center = FVector::DistSquared(center, FVector(0.0f));
			float end_to_center = FVector::DistSquared(end, FVector(0.0f));

			if (end_to_center < centroid_to_center)
				normal = -normal;
		}
		else {
			normal = FVector(0.0f);
		}
	}

	res = IPlane(FVector(0.0f), center, normal);
	return res;

}

FVector AIModel::getNormalsDominantVector(int gridSize, float voxelSize, float tolerance, int numIterations, bool draw, FColor color) {
	FColor red = FColor(150, 0, 0);
	FColor green = FColor(0, 100, 0);
	FColor blue = FColor(0, 0, 100);
	FColor black = FColor(0, 0, 0);

	VoxelGrid temp_grid;
	FVector res = FVector(0.0f);
	FVector object_center, temp_centroid, temp_vector;

	// NORMALS AUXILIARY
	int i, index, max_num_normals, second_max_num_normals, tolerance_num_normals, num_dominant_vectors;
	TArray<IPlane> planes;

	TArray<FVector> group_leaders;
	TArray<int> group_num_normals;

	TArray<FVector> dominant_group;
	TArray<FVector> dominant_group_leftover;

	int temp_num;
	FVector temp_normal;
	FVector temp_mirror;

	temp_grid = VoxelGrid(this->grid.getPosition(), gridSize, voxelSize);
	object_center = this->cloud.getCentroid();

	// VOXELIZE MODEL
	temp_grid.voxelize(this->cloud);

	// CALCULATE ESTIMATED PLANE FOR EACH VOXEL
	planes = temp_grid.getNormals(numIterations);

	// GROUP UP DIFFERENT EXISTENT NORMALS
	for (IPlane p : planes) {
		temp_normal = p.getNormal();

		if (!temp_normal.Equals(FVector(0.0f, 0.0f, 0.0f))) {
			if (!group_leaders.Contains(temp_normal)) {
				group_leaders.AddUnique(temp_normal);
				group_num_normals.Add(0);
			}

			group_leaders.Find(temp_normal, index);
			group_num_normals[index]++;
		}
	}

	// GET MAX NUM NORMALS
	max_num_normals = 0;
	second_max_num_normals = -1;
	for (int num : group_num_normals) {
		if (num > max_num_normals) {
			second_max_num_normals = max_num_normals;
			max_num_normals = num;
		}
	}

	tolerance_num_normals = max_num_normals - FMath::FloorToInt(max_num_normals * tolerance);
	// UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Max density is '%d' ..."), max_num_normals);
	// UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Tolerance density is '%d' ..."), tolerance_num_normals);
	// UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Second best value was '%d' ..."), second_max_num_normals);

	// GET DOMINANT GROUP
	i = 0;
	for (i = 0; i < group_num_normals.Num(); i++) {
		temp_num = group_num_normals[i];
		if (temp_num >= tolerance_num_normals) {
			temp_normal = group_leaders[i];
			dominant_group.AddUnique(temp_normal);
			dominant_group_leftover.AddUnique(temp_normal);
			// UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Normal[%d] = ( %.2f, %.2f, %.2f ) ..."), i, temp_normal.X, temp_normal.Y, temp_normal.Z);
		}
	}

	// CHECK FOR ANY SIMETRY/NULL VECTORS
	for (FVector v : dominant_group) {
		temp_mirror = -v;
		for (FVector m : dominant_group_leftover) {
			if (m.Equals(temp_mirror, 0.1f)) {
				dominant_group_leftover.Remove(v);
				dominant_group_leftover.Remove(m);
			}
		}
	}

	num_dominant_vectors = dominant_group_leftover.Num();

	// NO VECTORS LEFT - ANY VECTOR CAN BE A DOMINANT VECTOR
	if (num_dominant_vectors == 0) {
		// UE_LOG(LogTemp, Warning, TEXT("[DEBUG] NO VECTORS LEFT - ANY VECTOR CAN BE A DOMINANT VECTOR ..."));
		res = dominant_group[0];
	}
	// THERE IS 1 OR MORE VECTORS REMAINING - CALCULATING MEDIUM VECTOR
	else {
		// UE_LOG(LogTemp, Warning, TEXT("[DEBUG] THERE IS 1 OR MORE VECTORS REMAINING - CALCULATING MEDIUM VECTOR ..."));
		for (FVector v : dominant_group_leftover) {
			res += v;
		}

		res /= num_dominant_vectors;
	}

	if (draw) {
		// VISUAL DEBUG - DRAW CENTER AND VOXEL POINTS
		DrawDebugPoint(
			GetWorld(),
			object_center,
			4,  	// Size
			green,
			1000    // Duration
		);

		// VISUAL DEBUG - DRAW DOMINANT GROUP VECTORS
		for (FVector v : dominant_group) {
			if (!v.Equals(res)) {
				DrawDebugDirectionalArrow(
					GetWorld(),
					object_center,
					v * this->grid.getSize(),
					0.2f,
					blue,
					1000,
					-1.0f,
					(uint8)'\000',
					0.03f
				);
			}

			DrawDebugDirectionalArrow(
				GetWorld(),
				object_center,
				res * this->grid.getSize(),
				0.2f,
				color,
				1000,
				-1.0f,
				(uint8)'\000',
				0.1f
			);
		}
	}

	// UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Normal Dominant Vector = ( %.2f, %.2f, %.2f ) ..."), res.X, res.Y, res.Z);

	return res;
}

FVector AIModel::getDensityDominantVector(int gridSize, float voxelSize, float tolerance, bool draw, FColor color) {
	FColor red = FColor(150, 0, 0);
	FColor green = FColor(0, 100, 0);
	FColor blue = FColor(0, 0, 100);
	FColor black = FColor(0, 0, 0);

	FVector res = FVector(0.0f);
	VoxelGrid temp_grid;
	FVector object_center, temp_centroid, temp_vector;

	// DENSITY AUXILIARY
	int i, max_density, tolerance_density, num_dominant_vectors;
	TArray<Voxel> voxels_ordered_by_density;

	TArray<Voxel> dominant_group_voxels;
	TArray<FVector> dominant_group_normals;
	TArray<FVector> dominant_group_normals_leftover;

	FVector temp_normal;
	FVector temp_mirror;

	temp_grid = VoxelGrid(this->grid.getPosition(), gridSize, voxelSize);
	object_center = this->cloud.getCentroid();

	// VOXELIZE MODEL
	temp_grid.voxelize(this->cloud);

	// ORDER VOXELS BY DENSITY
	voxels_ordered_by_density = temp_grid.getVoxels();
	voxels_ordered_by_density.Sort([](Voxel v1, Voxel v2) {
		return v1.getNumPoints() > v2.getNumPoints();
	});

	// GET MAX DENSITY
	max_density = voxels_ordered_by_density[0].getNumPoints();
	tolerance_density = max_density - FMath::FloorToInt(max_density * tolerance);
	// UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Max density is '%d' ..."), max_density);
	// UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Tolerance density is '%d' ..."), tolerance_density);

	for (Voxel v : voxels_ordered_by_density) {
		if (v.getNumPoints() != max_density) {
			// UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Second best value was '%d' ..."), v.getNumPoints());
			break;
		}
	}

	// GET DOMINANT GROUP
	i = 0;
	for (Voxel v : voxels_ordered_by_density) {
		if (v.getNumPoints() >= tolerance_density) {
			dominant_group_voxels.Add(v);
			// UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Voxel[%d] = ( %d, %d, %d ) ..."), i, v.getI(), v.getJ(), v.getK());
			i++;
		}
		else break;
	}

	// CALCULATE DOMINANT GROUP VOXELS NORMALS
	i = 0;
	for (Voxel v : dominant_group_voxels) {
		temp_normal = v.getCentroid() - object_center;
		temp_normal.Normalize();
		dominant_group_normals.AddUnique(temp_normal);
		dominant_group_normals_leftover.AddUnique(temp_normal);
		// UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Normal[%d] = ( %.2f, %.2f, %.2f ) ..."), i, temp_normal.X, temp_normal.Y, temp_normal.Z);
		i++;
	}

	// CHECK FOR ANY SIMETRY/NULL VECTORS
	for (FVector v : dominant_group_normals) {
		temp_mirror = -v;
		for (FVector m : dominant_group_normals_leftover) {
			if (m.Equals(temp_mirror, 0.1f)) {
				dominant_group_normals_leftover.Remove(v);
				dominant_group_normals_leftover.Remove(m);
			}
		}
	}

	num_dominant_vectors = dominant_group_normals_leftover.Num();

	// NO VECTORS LEFT - ANY VECTOR CAN BE A DOMINANT VECTOR
	if (num_dominant_vectors == 0) {
		// UE_LOG(LogTemp, Warning, TEXT("[DEBUG] NO VECTORS LEFT - ANY VECTOR CAN BE A DOMINANT VECTOR ..."));
		res = dominant_group_normals[0];
	}
	// THERE IS 1 OR MORE VECTORS REMAINING - CALCULATING MEDIUM VECTOR
	else {
		// UE_LOG(LogTemp, Warning, TEXT("[DEBUG] THERE IS 1 OR MORE VECTORS REMAINING - CALCULATING MEDIUM VECTOR ..."));
		for (FVector v : dominant_group_normals_leftover) {
			res += v;
		}

		res /= num_dominant_vectors;
	}

	if (draw) {
		// VISUAL DEBUG - DRAW CENTER AND VOXEL POINTS
		DrawDebugPoint(
			GetWorld(),
			object_center,
			4,  	// Size
			green,
			1000    // Duration
		);

		// VISUAL DEBUG - DRAW DOMINANT GROUP VECTORS
		for (FVector v : dominant_group_normals) {
			if (!v.Equals(res)) {
				DrawDebugDirectionalArrow(
					GetWorld(),
					object_center,
					v * this->grid.getSize(),
					0.2f,
					blue,
					1000,
					-1.0f,
					(uint8)'\000',
					0.03f
				);
			}
		}

		// DRAW DOMINANT VECTOR
		DrawDebugDirectionalArrow(
			GetWorld(),
			object_center,
			res * this->grid.getSize(),
			0.2f,
			color,
			1000,
			-1.0f,
			(uint8)'\000',
			0.1f
		);
	}

	// UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Density Dominant Vector = ( %.2f, %.2f, %.2f ) ..."), res.X, res.Y, res.Z);

	return res;
}

void AIModel::alignVectors(FVector a, FVector b) {
	FQuat quat = FQuat::FindBetweenVectors(a, b); // Generates the 'smallest' (geodesic) rotation between two vectors of arbitrary length
	FVector rotation = -quat.Euler(); // Convert a Quaternion into floating-point Euler angles (in degrees) (Correct angle signal)

									  // UE_LOG(LogTemp, Warning, TEXT("[DEBUG] A = ( %.2f, %.2f, %.2f ) ..."), a.X, a.Y, a.Z);
	// UE_LOG(LogTemp, Warning, TEXT("[DEBUG] B = ( %.2f, %.2f, %.2f ) ..."), b.X, b.Y, b.Z);
	// UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Rotation = ( %.2f�, %.2f�, %.2f� ) ..."), rotation.X, rotation.Y, rotation.Z);

	// ROTATE X
	this->rotateX(rotation.X);

	// ROTATE Y
	this->rotateY(rotation.Y);

	// ROTATE Z
	this->rotateZ(rotation.Z);
}

// ------------------------- //
// ----- DEBUG METHODS ----- //
// ------------------------- //

// ----- SETUP ----- //
void AIModel::clear() {
	// GRID
	this->grid = VoxelGrid();

	// POINTS
	this->origin = CloudPoint();
	this->cloud = CloudPoint();

	// UNIT
	this->unit_x = 1.0f;
	this->unit_y = 1.0f;
	this->unit_z = 1.0f;
}

void AIModel::setup(int form, FVector position, float a, float b, int density, float gridSize, float voxelSize) {

	// CREATE LOCAL GENERATOR
	Generator gen = Generator();

	// GENERATE GRID
	this->grid = VoxelGrid(FVector(0.0f), gridSize, voxelSize);

	// GENERATE CLOUDPOINT
	switch(form) {
		// CUBE
		case 0:
			// a = size
			this->origin = gen.generateCube(position, a, density);
			break;
		// SPHERE
		case 1:
			// a = radius
			this->origin = gen.generateSphere(position, a, density);
			break;
		// CONE
		case 2:
			// a = radius
			// b = height
			this->origin = gen.generateCone(position, a, b, density);
			break;
		// CILINDER
		case 3:
			// a = radius
			// b = height
			this->origin = gen.generateCylinder(position, a, b, density);
			break;
		// PYRAMID
		case 4:
			// a = squareSize
			// b = height
			this->origin = gen.generatePyramid(position, a, b, density);
			break;
	}

	this->reset();
}

void AIModel::rebuildVoxels(CloudPoint c) {
	// Reset origin grid
	this->grid = VoxelGrid(FVector(0.0f), this->grid.getSize(), this->grid.getVoxelSize());

	// Voxelize model
	this->grid.voxelize(c);
}

// ----- GRID ----- //
void AIModel::drawGrid() {
	FColor red = FColor(150, 0, 0);
	FColor green = FColor(0, 100, 0);
	FColor blue = FColor(0, 0, 100);
	float gridSize = this->grid.getSize();
	float half_gridSize = gridSize * 0.5f;

	FVector center = this->grid.getPosition();
	FVector x = FVector(2 * gridSize, 0.0f, 0.0f);
	FVector y = FVector(0.0f, 2 * gridSize, 0.0f);
	FVector z = FVector(0.0f, 0.0f, 2 * gridSize);

	// UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Drawing grid model ..."));

	// Draw box
	DrawDebugBox(
		GetWorld(),
		this->grid.getPosition(),
		FVector(half_gridSize, half_gridSize, half_gridSize), // Extent
		red,
		1000    // Duration
	);

	// Draw orientation X, Y and Z

	// X
	DrawDebugDirectionalArrow(
		GetWorld(),
		center,
		center + x,
		2,
		red,
		1000
	);

	// Y
	DrawDebugDirectionalArrow(
		GetWorld(),
		center,
		center + y,
		2,
		green,
		1000
	);

	// Z
	DrawDebugDirectionalArrow(
		GetWorld(),
		center,
		center + z,
		2,
		blue,
		1000
	);

}

// ----- POINTS ----- //
void AIModel::drawPoints(FColor color) {
	// UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Drawing cloud point model ..."));
	for (FVector v : this->cloud.getCloud()) {
		DrawDebugPoint(
			GetWorld(),
			v,
			2,  	// Size
			color,
			1000    // Duration
		);
	}
}

// ----- VOXELIZATION ----- //
void AIModel::drawVoxels(bool goForOccupied) {
	FColor red = FColor(150, 0, 0);
	FColor green = FColor(0, 100, 0);
	FColor blue = FColor(0, 0, 100);
	float half_voxelSize = this->grid.getVoxelSize() * 0.5f;

	// UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Drawing voxels model ..."));
	for (Voxel v : this->grid.getVoxels()) {
		if (!goForOccupied || (goForOccupied && v.isOccupied())) {
			DrawDebugBox(
				GetWorld(),
				v.getCentroid(),
				FVector(half_voxelSize, half_voxelSize, half_voxelSize), // Extent
				blue,
				1000    // Duration
			);
		}
	}
}

// ----- OCCUPANCY ----- //
void AIModel::testGrid() {
	FColor red = FColor(150, 0, 0);
	FColor green = FColor(0, 100, 0);
	FColor blue = FColor(0, 0, 100);

	FVector voxelPos;
	float half_voxelSize = this->grid.getVoxelSize() * 0.5f;

	// Draw cloudpoint
	this->drawPoints(blue);

	// UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Testing model grid for occupancy ..."));

	for (Voxel v : this->grid.getVoxels()) {
		voxelPos = v.getCentroid();

		if (v.isOccupied()) {
			DrawDebugBox(
				GetWorld(),
				voxelPos,
				FVector(half_voxelSize, half_voxelSize, half_voxelSize), // Extent
				green,
				1000 	// Duration
			);
		} else {
			DrawDebugBox(
				GetWorld(),
				voxelPos,
				FVector(half_voxelSize, half_voxelSize, half_voxelSize), // Extent
				red,
				1000    // Duration
			);
		}

	}
}

// ----- NOISE ----- //
void AIModel::testNoise(float level) {
	FColor red = FColor(150, 0, 0);
	FColor green = FColor(0, 100, 0);
	FColor blue = FColor(0, 0, 100);
	float half_gridSize = this->grid.getSize() * 0.5f;

	CloudPoint noiseCloud;

	// Draw pointcloud
	this->drawPoints(blue);

	// BUILD TEMPORARY CLOUD
	noiseCloud = this->cloud.applyNoise(level);

	// UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Testing model noise ..."));
	for (FVector v : noiseCloud.getCloud()) {
		if (fabs(v.X) <= half_gridSize && fabs(v.Y) <= half_gridSize && fabs(v.Z) <= half_gridSize) {
			DrawDebugPoint(
				GetWorld(),
				v,
				2,  	// Size
				green,
				1000    // Duration
			);
		}
		else {
			DrawDebugPoint(
				GetWorld(),
				v,
				2,  	// Size
				red,
				1000    // Duration
			);
		}
	}
}

void AIModel::noise(float level) {
	CloudPoint noiseCloud = this->cloud.applyNoise(level);
	this->reset(noiseCloud);
}

// ----- NORMALS ----- //
void AIModel::showNormals(float gridSize, float voxelSize, int numIterations) {
	FColor red = FColor(150, 0, 0);
	FColor green = FColor(0, 100, 0);
	FColor blue = FColor(0, 0, 100);

	VoxelGrid temp_grid;
	TArray<IPlane> planes;

	// BUILD TEMPORARY GRID
	temp_grid = VoxelGrid(this->grid.getPosition(), gridSize, voxelSize);

	// VOXELIZE MODEL
	temp_grid.voxelize(this->cloud);

	// CALCULATE ESTIMATED PLANE FOR EACH VOXEL
	planes = temp_grid.getNormals(numIterations);

	// DRAW ESTIMATED PLANE FOR EACH VOXEL
	TArray<FVector> temp_points;
	TArray<FVector> temp_plane;
	FVector temp_position;
	FVector temp_centroid;
	FVector temp_normal;
	FVector temp_end;

	int i = 0;
	// UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Normals Length = %d"), planes.Num());

	for (IPlane p : planes) {
		temp_position = p.getPosition();
		temp_centroid = p.getCentroid();
		temp_normal = p.getNormal();
		// UE_LOG(LogTemp, Warning, TEXT("[DEBUG] NORMAL[%d] = ( %.2f, %.2f, %.2f )"), i, temp_normal.X, temp_normal.Y, temp_normal.Z);

		// Check if there is a normal to draw
		if (!temp_normal.IsZero()) {
			temp_end = temp_centroid + temp_normal;

			// Draw centroid
			DrawDebugPoint(
				GetWorld(),
				temp_centroid,
				6,  	// Size
				green,
				1000    // Duration
			);

			// Draw centroid + normal
			DrawDebugLine(
				GetWorld(),
				temp_centroid,
				temp_end,
				red,
				1000
			);
		}

		i++;
	}
}

// ----- ROTATION ----- //
void AIModel::showRotationInvariantOLLS(int numIterations, FColor color) {
	FVector ollsDominant;

	// Draw model points and grid
	this->drawPoints(color);

	// Calculate and draw olls dominant vector
	ollsDominant = this->getOLLSDominantVector(numIterations, true, color);
}

void AIModel::showRotationInvariantNormals(float gridSize, float voxelSize, float tolerance, int normalsIterations, FColor color) {
	FVector normalsDominant;

	// Draw model points and grid
	this->drawPoints(color);

	// Calculate and draw normal dominant vector
	normalsDominant = this->getNormalsDominantVector(gridSize, voxelSize, tolerance, normalsIterations, true, color);
}

void AIModel::showRotationInvariantDensity(float gridSize, float voxelSize, float tolerance, FColor color) {
	FVector densityDominant;

	// Draw model points and grid
	this->drawPoints(color);

	// Calculate and draw density dominant vector
	densityDominant = this->getDensityDominantVector(gridSize, voxelSize, tolerance, true, color);
}

// --------------------------- //
// ----- RESCONSTRUCTION ----- //
// --------------------------- //

void AIModel::mirrorRebuild() {
	CloudPoint res = CloudPoint();

	TArray<FVector> original;
	TArray<FVector> mirror;
	TArray<FVector> corrected;

	// Create original and mirror point set
	for (FVector v : this->cloud.getCloud()) {
		original.AddUnique(v);
		mirror.AddUnique(-v);
	}

	// Correct mirror
	corrected = this->mirrorCorrection(original, mirror);

	// Add points do cloud
	res.addPoints(corrected);

	this->reset(res);
}

TArray<FVector> AIModel::mirrorCorrection(TArray<FVector> a, TArray<FVector> b) {
	TArray<FVector> res;

	float step = this->grid.getSize() / 100;

	// Translate points while they touch each other
	while (this->pointsTouch(a, b)) {
		a = translate(a, -step, 0.0f, 0.0f);
		b = translate(b, step, 0.0f, 0.0f);
	}

	// Add points a
	for (FVector va : a) res.Add(va);

	// Add points b
	for (FVector vb : b) res.Add(vb);

	return res;
}

bool AIModel::pointsTouch(TArray<FVector> a, TArray<FVector> b) {
	bool res = false;
	float safeDistance = this->grid.getSize() / 20.0f;

	for (FVector va : a) {
		for (FVector vb : b) {
			if (FVector::Dist(va, vb) < safeDistance) {
				res = true;
				break;
			}
		}
	}

	return res;
}

// --------------------- //
// ----- SAVE DATA ----- //
// --------------------- //
void AIModel::saveModel(FString filename, FString description, int type, int rotationInvariant, bool mirrorRebuild, bool treatData) {
	FString path;

	TArray<int> oc;
	TArray<IPlane> no;

	// CENTER MODEL HERE!
	UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Centering model ..."));
	this->center();

	// NORMALIZE MODEL HERE!
	UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Normalizing model ..."));
	this->normalize();

	// MIRROR MODEL HERE!
	if (mirrorRebuild) {
		this->mirrorRebuild();
	}

	if (treatData) {
		// MAKE MODEL ROTATION HERE!
		UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Rotating model to invariant place ..."));
		this->rotateWithOlls(100, false, FColor::Black);

		switch (rotationInvariant) {
			case 0:
				this->rotateWithNormals(5.0f, 0.3f, 0.1f, 100, false, FColor::Black);
				break;
			case 1:
				this->rotateWithDensity(5.0f, 0.2f, 0.2f, false, FColor::Black);
				break;
		}
	}

	int i = 0;
	// Check model type
	switch (type) {
		case -1:
			UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Saving cloudpoint model ..."));

			path = FPaths::ConvertRelativePathToFull(FPaths::GameContentDir()) + "Resources/Models/" + filename;
			this->writeModelCloud(path, description);
			break;
		case 0:
			UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Saving occupancy model ..."));

			path = FPaths::ConvertRelativePathToFull(FPaths::GameContentDir()) + "Resources/Networks/Classification/Occupancy/Models/Test/" + filename;
			oc = this->grid.getOccupancy();
			this->writeModelLeNet(path, description, oc);
			break;
		case 1:
			UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Saving density model ..."));

			path = FPaths::ConvertRelativePathToFull(FPaths::GameContentDir()) + "Resources/Networks/Classification/Density/Models/Test/" + filename;
			oc = this->grid.getDensity();
			this->writeModelLeNet(path, description, oc);
			break;
		case 2:
			UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Saving normals model ..."));

			path = FPaths::ConvertRelativePathToFull(FPaths::GameContentDir()) + "Resources/Networks/Classification/Normals/Models/Test/" + filename;
			no = this->grid.getNormals(100);
			this->writeModelLeVector(path, description, no);
			break;
	}
}

void AIModel::createSet(FString filename, int label, FString description, int mode, int type, int numModels, float maxNoise) {
	FString path;

	TArray<TArray<int>> oc;
	TArray<TArray<IPlane>> no;

	VoxelGrid temp_grid;
	CloudPoint temp_cloud;

	float noise_step = maxNoise / numModels;

	UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Generating %d models with noise step = %.6f ..."), numModels, noise_step);

	// Check training mode
	FString trainingMode;
	switch (mode) {
		case 0:
			trainingMode = "Simple/";
			break;
		case 1:
			trainingMode = "Advanced/";
			break;
		case 2:
			trainingMode = "Custom/";
			break;
	}

	// Check model type
	float noise_value;

	switch (type) {
		case 0:
			UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Generating occupancy set ..."));

			path = FPaths::ConvertRelativePathToFull(FPaths::GameContentDir()) + "Resources/Networks/Classification/Occupancy/Models/Train/" + trainingMode;

			// Get new label value if label < 0
			if (label < 0) {
				label = this->calculateLabel(path, description);
			}

			for (int i = 0; i < numModels; i++) {
				noise_value = i * noise_step;
				temp_cloud = this->cloud.applyNoise(noise_value);

				// Rebuild voxelgrid
				temp_grid = VoxelGrid(this->grid.getPosition(), this->grid.getSize(), this->grid.getVoxelSize());
				temp_grid.voxelize(temp_cloud);
				oc.Add(temp_grid.getOccupancy());

				// Print similarity
				// similarity = this->grid.similarity(temp_grid, type);
				// UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Model[%d] is %.2f similar to the original ..."), i, similarity);
			}

			UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Saving occupancy set ..."));
			this->writeSetLeNet(path + filename, label, description, oc);
			break;
		case 1:
			UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Generating density set ..."));

			path = FPaths::ConvertRelativePathToFull(FPaths::GameContentDir()) + "Resources/Networks/Classification/Density/Models/Train/" + trainingMode;

			// Get new label value if label < 0
			if (label < 0) {
				label = this->calculateLabel(path, description);
			}

			for (int i = 0; i < numModels; i++) {
				noise_value = i * noise_step;
				temp_cloud = this->cloud.applyNoise(noise_value);

				// Rebuild voxelgrid
				temp_grid = VoxelGrid(this->grid.getPosition(), this->grid.getSize(), this->grid.getVoxelSize());
				temp_grid.voxelize(temp_cloud);
				oc.Add(temp_grid.getDensity());

				// Print similarity
				// similarity = this->grid.similarity(temp_grid, type);
				// UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Model[%d] is %.2f similar to the original ..."), i, similarity);
			}

			UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Saving density set ..."));
			this->writeSetLeNet(path + filename, label, description, oc);
			break;
		case 2:
			UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Generating normals set ..."));

			path = FPaths::ConvertRelativePathToFull(FPaths::GameContentDir()) + "Resources/Networks/Classification/Normals/Models/Train/" + trainingMode;

			// Get new label value if label < 0
			if (label < 0) {
				label = this->calculateLabel(path, description);
			}

			for (int i = 0; i < numModels; i++) {
				noise_value = i * noise_step;
				temp_cloud = this->cloud.applyNoise(noise_value);

				// Rebuild voxelgrid
				temp_grid = VoxelGrid(this->grid.getPosition(), this->grid.getSize(), this->grid.getVoxelSize());
				temp_grid.voxelize(temp_cloud);
				no.Add(temp_grid.getNormals(100));

				// Print similarity
				// similarity = this->grid.similarity(temp_grid, type);
				// UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Model[%d] is %.2f similar to the original ..."), i, similarity);
			}

			UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Saving normals set ..."));
			this->writeSetLeVector(path + filename, label, description, no);
			break;
	}

}

int AIModel::calculateLabel(FString path, FString description) {
	FString filePath = path + "classes.iordcore";
	FString descriptionToWrite = description + "\n";

	std::string line;
	std::string descriptionString(TCHAR_TO_UTF8(*descriptionToWrite));
	int lineNumber = 0;

	// Check if file exists, if not, create file
	std::ofstream classesFile;
	classesFile.open(*filePath, std::ios_base::app);
	classesFile << descriptionString;
	classesFile.close();

	// Write description to "classes.iordcore"
	std::ifstream existFile(*filePath);

	// Count file lines
	while (std::getline(existFile, line)) lineNumber++;
	existFile.close();

	UE_LOG(LogTemp, Warning, TEXT("[DEBUG] No label was given so new label is %d ..."), lineNumber - 1);
	return lineNumber - 1;
}

// ----- CLOUDPOINT ----- // - POINTS
void AIModel::writeModelCloud(FString path, FString description) {
	FString JsonStr;
	TSharedRef <TJsonWriter<TCHAR>> JsonWriter = TJsonWriterFactory<>::Create(&JsonStr);

	// Local data
	FVector tempNormal;
	TArray<float> x, y, z;

	// Write data
	JsonWriter->WriteObjectStart();

	// -----
	JsonWriter->WriteObjectStart("model");
	JsonWriter->WriteValue("description", description);
	JsonWriter->WriteValue("points", this->cloud.numPoints());

	for (FVector p : this->cloud.getCloud()) {
		x.Add(p.X);
		y.Add(p.Y);
		z.Add(p.Z);
	}

	// LIST X
	JsonWriter->WriteArrayStart("x");
	for (float xValue : x) {
		JsonWriter->WriteValue((TEXT("%.2f"), xValue));
	}
	JsonWriter->WriteArrayEnd();

	// LIST Y
	JsonWriter->WriteArrayStart("y");
	for (float yValue : y) {
		JsonWriter->WriteValue((TEXT("%.2f"), yValue));
	}
	JsonWriter->WriteArrayEnd();

	// LIST Z
	JsonWriter->WriteArrayStart("z");
	for (float zValue : z) {
		JsonWriter->WriteValue((TEXT("%.2f"), zValue));
	}
	JsonWriter->WriteArrayEnd();

	JsonWriter->WriteObjectEnd();
	// -----

	JsonWriter->WriteObjectEnd();
	JsonWriter->Close();

	// Save string
	UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Writing to file '%s'"), *path);
	FFileHelper::SaveStringToFile(*JsonStr, *path);
}

void AIModel::loadModelCloud(FString filename, float gridSize, float voxelSize) {
	FString path = FPaths::ConvertRelativePathToFull(FPaths::GameContentDir()) + "/Resources/Models/" + filename;
	UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Loading cloudpoint model from '%s' ..."), *path);

	FString JsonStr;
	TSharedPtr<FJsonObject> JsonObject = MakeShareable(new FJsonObject());

	FFileHelper::LoadFileToString(JsonStr, *path);
	TSharedRef<TJsonReader<TCHAR>> JsonReader = TJsonReaderFactory<>::Create(*JsonStr);

	FJsonSerializer::Deserialize(JsonReader, JsonObject);
	if (JsonObject.IsValid())
	{
		auto model = JsonObject->GetObjectField(TEXT("model"));
		FString description = model->GetStringField(TEXT("description"));

		TArray<FVector> points;
		TArray<float> x, y, z;

		auto xRaw = model->GetArrayField(TEXT("x"));
		auto yRaw = model->GetArrayField(TEXT("y"));
		auto zRaw = model->GetArrayField(TEXT("z"));

		// Load x list
		for (TSharedPtr<FJsonValue> xValue : xRaw) {
			x.Add(xValue->AsNumber());
		}

		// Load y list
		for (TSharedPtr<FJsonValue> yValue : yRaw) {
			y.Add(yValue->AsNumber());
		}

		// Load z list
		for (TSharedPtr<FJsonValue> zValue : zRaw) {
			z.Add(zValue->AsNumber());
		}

		CloudPoint res = CloudPoint();

		int numPoints = x.Num();
		for (int i = 0; i < numPoints; i++) {
			res.addPoint(FVector(x[i], y[i], z[i]));
		}

		// GENERATE GRID
		this->grid = VoxelGrid(FVector(0.0f), gridSize, voxelSize);

		// CONFIGURE ORIGIN
		this->origin = res;

		// CLEAR ALL THINGS UP
		this->reset();
	}
}

// ----- LE NET ----- // - OCCUPANCY + DENSITY
void AIModel::writeModelLeNet(FString path, FString description, TArray<int> list) {
	FString JsonStr;
	TSharedRef <TJsonWriter<TCHAR>> JsonWriter = TJsonWriterFactory<>::Create(&JsonStr);

	// Write data
	JsonWriter->WriteObjectStart();

	// -----
	JsonWriter->WriteObjectStart("model");
	JsonWriter->WriteValue("description", description);

	JsonWriter->WriteArrayStart("values");
	for (int i : list) {
		JsonWriter->WriteValue((TEXT("%d"), i));
	}
	JsonWriter->WriteArrayEnd();

	JsonWriter->WriteObjectEnd();
	// -----

	JsonWriter->WriteObjectEnd();
	JsonWriter->Close();

	// Save string
	UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Writing to file '%s'"), *path);
	FFileHelper::SaveStringToFile(*JsonStr, *path);
}

void AIModel::writeSetLeNet(FString path, int label, FString description, TArray<TArray<int>> dimension) {
	FString JsonStr;
	TSharedRef <TJsonWriter<TCHAR>> JsonWriter = TJsonWriterFactory<>::Create(&JsonStr);

	int i = 0;
	int count = dimension.Num();

	JsonWriter->WriteObjectStart();

	// -----
	JsonWriter->WriteObjectStart("model");

	JsonWriter->WriteValue("label", label);
	JsonWriter->WriteValue("description", description);

	JsonWriter->WriteValue("count", count);
	JsonWriter->WriteObjectStart("set");
	for (TArray<int> oc : dimension) {
		JsonWriter->WriteArrayStart(FString::FromInt(i));
		for (int j : oc) {
			JsonWriter->WriteValue((TEXT("%d"), j));
		}
		JsonWriter->WriteArrayEnd();

		i++;
	}
	JsonWriter->WriteObjectEnd();

	JsonWriter->WriteObjectEnd();
	// -----

	JsonWriter->WriteObjectEnd();
	JsonWriter->Close();

	// Save string
	UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Writing to file '%s'"), *path);
	FFileHelper::SaveStringToFile(*JsonStr, *path);
}

// ----- LE VECTOR ----- // - NORMALS
void AIModel::writeModelLeVector(FString path, FString description, TArray<IPlane> list) {
	FString JsonStr;
	TSharedRef <TJsonWriter<TCHAR>> JsonWriter = TJsonWriterFactory<>::Create(&JsonStr);

	// Local data
	FVector tempNormal;
	TArray<float> x, y, z;

	// Write data
	JsonWriter->WriteObjectStart();

	// -----
	JsonWriter->WriteObjectStart("model");
	JsonWriter->WriteValue("description", description);

	for (IPlane p : list) {
		tempNormal = p.getNormal();

		x.Add(tempNormal.X);
		y.Add(tempNormal.Y);
		z.Add(tempNormal.Z);
	}

	// LIST X
	JsonWriter->WriteArrayStart("x");
	for (float xValue : x) {
		JsonWriter->WriteValue((TEXT("%.2f"), xValue));
	}
	JsonWriter->WriteArrayEnd();

	// LIST Y
	JsonWriter->WriteArrayStart("y");
	for (float yValue : y) {
		JsonWriter->WriteValue((TEXT("%.2f"), yValue));
	}
	JsonWriter->WriteArrayEnd();

	// LIST Z
	JsonWriter->WriteArrayStart("z");
	for (float zValue : z) {
		JsonWriter->WriteValue((TEXT("%.2f"), zValue));
	}
	JsonWriter->WriteArrayEnd();

	JsonWriter->WriteObjectEnd();
	// -----

	JsonWriter->WriteObjectEnd();
	JsonWriter->Close();

	// Save string
	UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Writing to file '%s'"), *path);
	FFileHelper::SaveStringToFile(*JsonStr, *path);
}

void AIModel::writeSetLeVector(FString path, int label, FString description, TArray<TArray<IPlane>> dimension) {
	FString JsonStr;
	TSharedRef <TJsonWriter<TCHAR>> JsonWriter = TJsonWriterFactory<>::Create(&JsonStr);

	int i = 0;
	int count = dimension.Num();

	// Write data
	JsonWriter->WriteObjectStart();

	// -----
	JsonWriter->WriteObjectStart("model");

	JsonWriter->WriteValue("label", label);
	JsonWriter->WriteValue("description", description);

	JsonWriter->WriteValue("count", count);
	JsonWriter->WriteObjectStart("set");
	for (TArray<IPlane> on : dimension) {
		// Calculate x, y, z lists
		TArray<float> x, y, z;
		FVector tempNormal;

		for (IPlane p : on) {
			tempNormal = p.getNormal();

			x.Add(tempNormal.X);
			y.Add(tempNormal.Y);
			z.Add(tempNormal.Z);
		}

		// Write object i
		JsonWriter->WriteObjectStart(FString::FromInt(i));

			// LIST X
			JsonWriter->WriteArrayStart("x");
			for (float xValue : x) {
				JsonWriter->WriteValue((TEXT("%.2f"), xValue));
			}
			JsonWriter->WriteArrayEnd();

			// LIST Y
			JsonWriter->WriteArrayStart("y");
			for (float yValue : y) {
				JsonWriter->WriteValue((TEXT("%.2f"), yValue));
			}
			JsonWriter->WriteArrayEnd();

			// LIST Z
			JsonWriter->WriteArrayStart("z");
			for (float zValue : z) {
				JsonWriter->WriteValue((TEXT("%.2f"), zValue));
			}
			JsonWriter->WriteArrayEnd();

		JsonWriter->WriteObjectEnd();
		i++;
	}
	JsonWriter->WriteObjectEnd();

	JsonWriter->WriteObjectEnd();
	// -----

	JsonWriter->WriteObjectEnd();
	JsonWriter->Close();

	// Save string
	UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Writing to file '%s'"), *path);
	FFileHelper::SaveStringToFile(*JsonStr, *path);
}

// ---------------------- //
// ----- ALGORITHMS ----- //
// ---------------------- //

// ----- CLASSIFICATION - ITERATIVE CLOSEST POINT (ICP) ----- //
/*
#include <tuple>
#include <stdlib.h>
#include <math.h>

#define MIN(x,y) ( (x) < (y) ? (x) : (y) )
#define MAX(x,y) ((x)>(y)?(x):(y))
#define SIGN(a, b) ((b) >= 0.0 ? fabs(a) : -fabs(a))

void AIModel::reconstruct(int maxIterationsPerModel, float minError, int numberPoints, TArray<AIModel> aux_models) {
	// ITERATIVE CLOSEST POINT
	TArray<FVector> model = this->cloud.getCloud();
	TArray<float> match_percs;

	// calculate match percentages
	for (AIModel m : aux_models) {
		float err = FLT_MAX;

		for (int i = 0; i < maxIterationsPerModel || err <= minError; i++) {
			// calculate n closest points
			TArray<std::tuple<FVector, FVector, float>> nearPoints;

			for (FVector p1 : model) {
				for (FVector p2 : m.cloud.getCloud()) {
					std::tuple<FVector, FVector, float> pair = std::make_tuple(p1, p2, FVector::Dist(p1, p2));
					nearPoints.Add(pair);
				}
			}
			nearPoints.Sort(
				[](std::tuple<FVector, FVector, float> & i, std::tuple<FVector, FVector, float> & j)
				-> bool {
				return (std::get<2>(i) < std::get<2>(j));
			});
			nearPoints.SetNum(numberPoints);

			// calculate best transformation
			// 1 - calculate centroids
			FVector centroidA;
			FVector centroidB;
			float xA = 0, yA = 0, zA = 0;
			float xB = 0, yB = 0, zB = 0;

			for (int i = 0; i < nearPoints.Num(); i++) {
				xA += std::get<0>(nearPoints[i]).X;
				yA += std::get<0>(nearPoints[i]).Y;
				zA += std::get<0>(nearPoints[i]).Z;
				xB += std::get<1>(nearPoints[i]).X;
				yB += std::get<1>(nearPoints[i]).Y;
				zB += std::get<1>(nearPoints[i]).Z;
			}

			centroidA.X = xA / nearPoints.Num();
			centroidA.Y = yA / nearPoints.Num();
			centroidA.Z = zA / nearPoints.Num();
			centroidB.X = xB / nearPoints.Num();
			centroidB.Y = yB / nearPoints.Num();
			centroidB.Z = zB / nearPoints.Num();

			// 2 - get translation vector (cA + T = cB)
			FVector translation(centroidB.X - centroidA.X, centroidB.Y - centroidA.Y, centroidB.Z - centroidA.Z);

			// 3 - get co-variance matrix
			float **covariance = new float *[nearPoints.Num()];
			for (int i = 0; i < nearPoints.Num(); i++) {
				covariance[i] = new float[3];
				for (int k = 0; k < nearPoints.Num(); k++) {
					covariance[i][0] = std::get<0>(nearPoints[k]).X * std::get<1>(nearPoints[k]).X;
					covariance[i][1] = std::get<0>(nearPoints[k]).Y * std::get<1>(nearPoints[k]).Y;
					covariance[i][2] = std::get<0>(nearPoints[k]).Z * std::get<1>(nearPoints[k]).Z;
				}
			}

			// 4 - apply svd algorithm to decompose co-variance matrix in [U,S,V] matrices
			float *w = new float[nearPoints.Num()];
			float **v = new float *[3];
			for (int k = 0; k < 3; k++)
				v[i] = new float[3];

			svd(covariance, nearPoints.Num(), 3, w, v);

			// 5 - calculate rotation matrix
			// R = V * U_t
			float **u = covariance;
			float **u_T = new float *[nearPoints.Num()];
			for (int k = 0; k < nearPoints.Num(); k++)
				u_T[i] = new float[nearPoints.Num()];
			float **rotation_matrix = new float *[4];
			for (int k = 0; k < nearPoints.Num(); k++)
				rotation_matrix[i] = new float[4];

			transpose(u, u_T, nearPoints.Num(), 3);
			matrix_multiply(rotation_matrix, v, u_T, 3, 3, 3);

			rotation_matrix[0][3] = 0;
			rotation_matrix[1][3] = 0;
			rotation_matrix[2][3] = 0;
			for (int i = 0; i < 3; i++)
				rotation_matrix[3][i] = 0;
			rotation_matrix[3][3] = 1;

			// transform using translation and rotation calculated
			this->translate(translation.X, translation.Y, translation.Z);

			FMatrix rotation;
			for (int i = 0; i < 4; i++)
				for (int j = 0; j < 4; j++)
					rotation.M[i][j] = rotation_matrix[i][j];
			FQuat rotation_quat(rotation);
			FVector angles = rotation_quat.Euler();

			this->rotateX(angles.X);
			this->rotateY(angles.Y);
			this->rotateZ(angles.Z);

			// calculate error
			err = errorFunction(nearPoints);
		}
		// calculate match percentage
		match_percs.Add(matchPercentage(this->cloud, m.cloud));
	}
	// choose best match
	int i, best_per = 0, index;
	for (i = 0; i < match_percs.Num(); i++) {
		float f = match_percs[i];
		if (f > best_per) {
			best_per = f;
			index = i;
		}
	}
	AIModel best_match = aux_models[i];

	// reconstruct original model using best match
	///////////////////////////////////////////////////////////// NOTA : nao e preciso rodar os pontos do novo para bater no A?
	for (FVector p : best_match.cloud.getCloud()) {
		if (this->cloud.getCloud().Find(p) == INDEX_NONE)
			this->cloud.getCloud().Add(p);
	}
}

void AIModel::transpose(float **a, float **b, int m, int n) {
	for (int i = 0; i < m; i++) {
		for (int j = 0; j < n; j++) {
			b[i][j] = a[j][i];
		}
	}
}

void AIModel::matrix_multiply(float **a, float **b, float **c, int m, int n, int o) {
	float sum;
	for (int i = 0; i < m; i++) {
		for (int j = 0; j < n; j++) {
			sum = 0;
			for (int k = 0; k < o; k++) {
				sum += a[i][k] * b[k][j];
			}
			c[i][j] = sum;
		}
	}
}

float AIModel::errorFunction(TArray<std::tuple<FVector, FVector, float>> points) {
	float error = 0;

	// ...

	return error;
}

float AIModel::matchPercentage(CloudPoint a, CloudPoint b) {
	int points;
	float match_per, match_points = 0;
	points = a.getCloud().Num();
	for (FVector p : a.getCloud()) {
		if (b.getCloud().Find(p) != INDEX_NONE)
			match_points++;
	}
	match_per = match_points / points;
}

float AIModel::PYTHAG(float a, float b) {
	float at = fabs(a), bt = fabs(b), ct, result;

	if (at > bt) {
		ct = bt / at;
		result = at * sqrt(1.0 + ct * ct);
	}
	else
		if (bt > 0.0) {
			ct = at / bt;
			result = bt * sqrt(1.0 + ct * ct);
		}
		else
			result = 0.0;
	return(result);
}

int AIModel::svd(float **a, int m, int n, float *w, float **v) {
	int flag, i, its, j, jj, k, l, nm;
	float c, f, h, s, x, y, z;
	float anorm = 0.0, g = 0.0, scale = 0.0;
	float *rv1;

	if (m < n) {
		return(0);
	}

	rv1 = (float *)malloc((unsigned int)n * sizeof(float));

	// Householder reduction to bidiagonal form
	for (i = 0; i < n; i++) {
		// left-hand reduction
		l = i + 1;
		rv1[i] = scale * g;
		g = s = scale = 0.0;
		if (i < m) {
			for (k = i; k < m; k++)
				scale += fabs((float)a[k][i]);
			if (scale) {
				for (k = i; k < m; k++) {
					a[k][i] = (float)((float)a[k][i] / scale);
					s += ((float)a[k][i] * (float)a[k][i]);
				}
				f = (float)a[i][i];
				g = -SIGN(sqrt(s), f);
				h = f * g - s;
				a[i][i] = (float)(f - g);
				if (i != n - 1) {
					for (j = l; j < n; j++) {
						for (s = 0.0, k = i; k < m; k++)
							s += ((float)a[k][i] * (float)a[k][j]);
						f = s / h;
						for (k = i; k < m; k++)
							a[k][j] += (float)(f * (float)a[k][i]);
					}
				}
				for (k = i; k < m; k++)
					a[k][i] = (float)((float)a[k][i] * scale);
			}
		}
		w[i] = (float)(scale * g);

		// right-hand reduction
		g = s = scale = 0.0;
		if (i < m && i != n - 1) {
			for (k = l; k < n; k++)
				scale += fabs((float)a[i][k]);
			if (scale) {
				for (k = l; k < n; k++) {
					a[i][k] = (float)((float)a[i][k] / scale);
					s += ((float)a[i][k] * (float)a[i][k]);
				}
				f = (float)a[i][l];
				g = -SIGN(sqrt(s), f);
				h = f * g - s;
				a[i][l] = (float)(f - g);
				for (k = l; k < n; k++)
					rv1[k] = (float)a[i][k] / h;
				if (i != m - 1) {
					for (j = l; j < m; j++) {
						for (s = 0.0, k = l; k < n; k++)
							s += ((float)a[j][k] * (float)a[i][k]);
						for (k = l; k < n; k++)
							a[j][k] += (float)(s * rv1[k]);
					}
				}
				for (k = l; k < n; k++)
					a[i][k] = (float)((float)a[i][k] * scale);
			}
		}
		anorm = MAX(anorm, (fabs((float)w[i]) + fabs(rv1[i])));
	}

	// accumulate the right-hand transformation
	for (i = n - 1; i >= 0; i--) {
		if (i < n - 1) {
			if (g) {
				for (j = l; j < n; j++)
					v[j][i] = (float)(((float)a[i][j] / (float)a[i][l]) / g);
				// float division to avoid underflow
				for (j = l; j < n; j++) {
					for (s = 0.0, k = l; k < n; k++)
						s += ((float)a[i][k] * (float)v[k][j]);
					for (k = l; k < n; k++)
						v[k][j] += (float)(s * (float)v[k][i]);
				}
			}
			for (j = l; j < n; j++)
				v[i][j] = v[j][i] = 0.0;
		}
		v[i][i] = 1.0;
		g = rv1[i];
		l = i;
	}

	// accumulate the left-hand transformation
	for (i = n - 1; i >= 0; i--) {
		l = i + 1;
		g = (float)w[i];
		if (i < n - 1)
			for (j = l; j < n; j++)
				a[i][j] = 0.0;
		if (g) {
			g = 1.0 / g;
			if (i != n - 1) {
				for (j = l; j < n; j++) {
					for (s = 0.0, k = l; k < m; k++)
						s += ((float)a[k][i] * (float)a[k][j]);
					f = (s / (float)a[i][i]) * g;
					for (k = i; k < m; k++)
						a[k][j] += (float)(f * (float)a[k][i]);
				}
			}
			for (j = i; j < m; j++)
				a[j][i] = (float)((float)a[j][i] * g);
		}
		else {
			for (j = i; j < m; j++)
				a[j][i] = 0.0;
		}
		++a[i][i];
	}

	// diagonalize the bidiagonal form
	for (k = n - 1; k >= 0; k--) {                             // loop over singular values
		for (its = 0; its < 30; its++) {                         // loop over allowed iterations
			flag = 1;
			for (l = k; l >= 0; l--) {                     // test for splitting
				nm = l - 1;
				if (fabs(rv1[l]) + anorm == anorm) {
					flag = 0;
					break;
				}
				if (fabs((float)w[nm]) + anorm == anorm)
					break;
			}
			if (flag) {
				c = 0.0;
				s = 1.0;
				for (i = l; i <= k; i++) {
					f = s * rv1[i];
					if (fabs(f) + anorm != anorm) {
						g = (float)w[i];
						h = PYTHAG(f, g);
						w[i] = (float)h;
						h = 1.0 / h;
						c = g * h;
						s = (-f * h);
						for (j = 0; j < m; j++) {
							y = (float)a[j][nm];
							z = (float)a[j][i];
							a[j][nm] = (float)(y * c + z * s);
							a[j][i] = (float)(z * c - y * s);
						}
					}
				}
			}
			z = (float)w[k];
			if (l == k) {                  // convergence
				if (z < 0.0) {              // make singular value nonnegative
					w[k
					] = (float)(-z);
					for (j = 0; j < n; j++)
						v[j][k] = (-v[j][k]);
				}
				break;
			}
			if (its >= 30) {
				free((void*)rv1);
				fprintf(stderr, "No convergence after 30,000! iterations \n");
				return(0);
			}

			//  shift from bottom 2 x 2 minor
			x = (float)w[l];
			nm = k - 1;
			y = (float)w[nm];
			g = rv1[nm];
			h = rv1[k];
			f = ((y - z) * (y + z) + (g - h) * (g + h)) / (2.0 * h * y);
			g = PYTHAG(f, 1.0);
			f = ((x - z) * (x + z) + h * ((y / (f + SIGN(g, f))) - h)) / x;

			//  next QR transformation
			c = s = 1.0;
			for (j = l; j <= nm; j++) {
				i = j + 1;
				g = rv1[i];
				y = (float)w[i];
				h = s * g;
				g = c * g;
				z = PYTHAG(f, h);
				rv1[j] = z;
				c = f / z;
				s = h / z;
				f = x * c + g * s;
				g = g * c - x * s;
				h = y * s;
				y = y * c;
				for (jj = 0; jj < n; jj++) {
					x = (float)v[jj][j];
					z = (float)v[jj][i];
					v[jj][j] = (float)(x * c + z * s);
					v[jj][i] = (float)(z * c - x * s);
				}
				z = PYTHAG(f, h);
				w[j] = (float)z;
				if (z) {
					z = 1.0 / z;
					c = f * z;
					s = h * z;
				}
				f = (c * g) + (s * y);
				x = (c * y) - (s * g);
				for (jj = 0; jj < m; jj++) {
					y = (float)a[jj][j];
					z = (float)a[jj][i];
					a[jj][j] = (float)(y * c + z * s);
					a[jj][i] = (float)(z * c - y * s);
				}
			}
			rv1[l] = 0.0;
			rv1[k] = f;
			w[k] = (float)x;
		}
	}
	free((void*)rv1);
	return(1);
} */
