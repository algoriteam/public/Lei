// Fill out your copyright notice in the Description page of Project Settings.

#include "IORD.h"
#include "VoxelGrid.h"

// ----- CONSTRUCTORS ----- //

VoxelGrid::VoxelGrid()
{
	// Nothing to do
}

VoxelGrid::VoxelGrid(FVector p, float s, float vs)
{
	this->pos = p;
	this->size = s;
	this->voxelSize = vs;

	// Pre-generate all voxels
	this->generateAllVoxels();
}

VoxelGrid::~VoxelGrid()
{
	// Clear memory
	this->clear();
}

// ----- METHODS ----- //

void VoxelGrid::voxelize(CloudPoint c)
{
	int i, j, k;
	
	// RESET VOXEL POINTS
	this->clearAllVoxels();

	for (FVector v : c.getCloud()) {
		i = v.X / this->voxelSize;
		j = v.Y / this->voxelSize;
		k = v.Z / this->voxelSize;

		this->addToVoxel(v, i, j, k);
	}
}

FVector VoxelGrid::getPosition() {
	return this->pos;
}

TArray<Voxel> VoxelGrid::getVoxels()
{
	return this->voxels;
}

Voxel VoxelGrid::getVoxel(int position) {
	return this->voxels[position];
}

float VoxelGrid::getSize() {
	return this->size;
}

float VoxelGrid::getVoxelSize() {
	return this->voxelSize;
}

float VoxelGrid::getMaximum() {
	FVector move = FVector(this->size * 0.5f, 0.0f, 0.0f);
	FVector limit = this->pos + move;
	float max = FMath::Max3(limit.X, limit.Y, limit.Z);

	return max;
}

TArray<int> VoxelGrid::getOccupancy() {
	TArray<int> res;

	for (Voxel v : this->voxels) {
		if (v.isOccupied()) res.Add(1);
		else res.Add(0);
	}

	return res;
}

TArray<int> VoxelGrid::getDensity() {
	TArray<int> res;

	for (Voxel v : this->voxels) {
		res.Add(v.getNumPoints());
	}

	return res;
}

TArray<IPlane> VoxelGrid::getNormals(int numIterations) {
	TArray<IPlane> res;

	// CALCULATE ESTIMATED PLANE FOR EACH VOXEL
	TArray<FVector> temp_points;
	IPlane temp_plane;
	FVector temp_position;
	FVector temp_centroid;
	FVector temp_normal;

	int i = 0;
	for (Voxel v : this->voxels) {
		// Calculat plane only if voxel has at least 3 points
		if (v.getNumPoints() >= 3) {
			// Retrieve voxel points
			temp_points = v.getPoints();

			// Calculate ordinary linear least squares plane (OLLSP)
			temp_plane = this->FindOLLSPlane(v.getCentroid(), temp_points, numIterations);
			temp_position = temp_plane.getPosition();
			temp_centroid = temp_plane.getCentroid();
			temp_normal = temp_plane.getNormal();
		}
		else {
			temp_position = FVector(0.0f);
			temp_centroid = FVector(0.0f);
			temp_normal = FVector(0.0f);
			temp_plane = IPlane(temp_position, temp_centroid, temp_normal);
		}

		res.Add(temp_plane);
		// UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Plane[%d] = < (%.2f, %.2f, %.2f) , (%.2f, %.2f, %.2f) , (%.2f, %.2f, %.2f) >"), i, temp_position.X, temp_position.Y, temp_position.Z, temp_centroid.X, temp_centroid.Y, temp_centroid.Z, temp_normal.X, temp_normal.Y, temp_normal.Z);
		i++;

	}

	return res;
}

float VoxelGrid::similarity(VoxelGrid v, int type) {
	TArray<int> originInt;
	TArray<int> noiseInt;

	TArray<IPlane> originNormals;
	TArray<IPlane> noiseNormals;

	switch (type) {
		case 0:
			originInt = this->getOccupancy();
			noiseInt = v.getOccupancy();
			break;
		case 1:
			originInt = this->getDensity();
			noiseInt = v.getDensity();
			break;
		case 2:
			originNormals = this->getNormals(100);
			noiseNormals = v.getNormals(100);
			break;
	}

	float equal = 0;
	float s;

	int a, b;
	FVector tempA, tempB;

	if (type < 2) {
		s = originInt.Num();

		for (int i = 0; i < s; i++) {
			a = originInt[i];
			b = noiseInt[i];

			if (a == b) equal++;
		}
	}
	else {
		s = originNormals.Num();

		for (int i = 0; i < s; i++) {
			tempA = originNormals[i].getNormal();
			tempB = noiseNormals[i].getNormal();

			if (tempA.Equals(tempB)) equal++;
		}
	}

	return equal / s;
}

void VoxelGrid::clear()
{
	this->voxels.Empty();
}

// ----- AUXILIARY METHODS ----- //

void VoxelGrid::generateAllVoxels()
{
	float real_size = this->size / this->voxelSize;
	int half_real_size = FMath::FloorToInt(real_size * 0.5f);

	int start_x = this->pos.X - half_real_size;
	int end_x = this->pos.X + half_real_size;
	int start_y = this->pos.Y - half_real_size;
	int end_y = this->pos.Y + half_real_size;
	int start_z = this->pos.Z - half_real_size;
	int end_z = this->pos.Z + half_real_size;

	int c = 0;
	for (int i = start_x; i <= end_x; i++) {
		for (int j = start_y; j <= end_y; j++) {
			for (int k = start_z; k <= end_z; k++) {
				this->addVoxel(Voxel(i, j, k, this->voxelSize));
				c++;
			}
		}
	}
}

void VoxelGrid::clearAllVoxels()
{
	for (Voxel v : this->voxels) {
		v.clearPoints();
	}
}

void VoxelGrid::addVoxel(Voxel v)
{
	this->voxels.AddUnique(v);
}

void VoxelGrid::removeVoxel(Voxel v)
{
	this->voxels.Remove(v);
}

void VoxelGrid::addToVoxel(FVector point, int i, int j, int k)
{
	int index;
	Voxel v = Voxel(i, j, k);
	bool found = this->voxels.Find(v, index);

	if (found) this->voxels[index].addPoint(point);
}

IPlane VoxelGrid::FindOLLSPlane(FVector position, TArray<FVector> points, int numIterations) {
	// <Centroid, Normal>
	IPlane res;

	int count = points.Num();
	Matrix33 mat;

	double sumX = 0.0;
	double sumY = 0.0;
	double sumZ = 0.0;

	FVector center;
	FVector normal;
	FVector end;

	// Calculate points sum
	for (int i = 0; i < count; i++) {
		sumX += points[i].X;
		sumY += points[i].Y;
		sumZ += points[i].Z;
	}

	// Calculate points centroid
	sumX /= count;
	sumY /= count;
	sumZ /= count;
	center = FVector(sumX, sumY, sumZ);

	double sumXX = 0.0, sumXY = 0.0, sumXZ = 0.0;
	double sumYY = 0.0, sumYZ = 0.0;
	double sumZZ = 0.0;

	double diffX, diffY, diffZ;

	for (int i = 0; i < count; i++) {
		diffX = points[i].X - center.X;
		diffY = points[i].Y - center.Y;
		diffZ = points[i].Z - center.Z;

		sumXX += diffX * diffX;
		sumXY += diffX * diffY;
		sumXZ += diffX * diffZ;
		sumYY += diffY * diffY;
		sumYZ += diffY * diffZ;
		sumZZ += diffZ * diffZ;
	}

	// Prepare matrix for eigenvectors calculation
	mat = Matrix33(sumXX, sumXY, sumXZ,
		sumXY, sumYY, sumYZ,
		sumXZ, sumYZ, sumZZ);

	/* TEST MATRICES
	Matrix33 mat1 = Matrix33(6,1,1,
	4,-2,5,
	2,8,7);

	Matrix33 mat2 = Matrix33(1, 2, 3,
	4, 5, 6,
	7, 8, 9);

	Matrix33 scaleMatrix = mat1.scale(2.0f);
	Matrix33 multiplyMatrix = mat1.multiply(mat2);
	FVector multiplyVector = mat1.multiply(FVector(1, 2, 3));
	float determinant = mat1.getDeterminant();
	Matrix33 inverse = mat1.getInverse();

	UE_LOG(LogTemp, Warning, TEXT("[DEBUG] original: %s"), *mat1.print()); // { 6.00, 1.00, 1.00, 4.00, -2.00, 5.00, 2.00, 8.00, 7.00 }
	UE_LOG(LogTemp, Warning, TEXT("[DEBUG] determinant: %.6f"), determinant); // -306
	UE_LOG(LogTemp, Warning, TEXT("[DEBUG] inverse: %s"), *inverse.print());
	UE_LOG(LogTemp, Warning, TEXT("[DEBUG] scaleMatrix: %s"), *scaleMatrix.print()); // { 12.00, 2.00, 2.00, 8.00, -4.00, 10.00, 4.00, 16.00, 14.00 }
	UE_LOG(LogTemp, Warning, TEXT("[DEBUG] multiplyMatrix: %s"), *multiplyMatrix.print()); // { 17.00, 25.00, 33.00, 31.00, 38.00, 45.00, 83.00, 100.00, 117.00 }
	UE_LOG(LogTemp, Warning, TEXT("[DEBUG] multiplyVector: %s"), *multiplyVector.ToString()); // X=11.000 Y=15.000 Z=39.000
	*/

	// Calculate determinant
	double det = mat.getDeterminant();

	if (det < 1e-16f) {
		normal = FVector(0.0f);
	}
	else {
		Matrix33 matInverse = mat.getInverse();
		normal = matInverse.findEigenVectorAssociatedWithLargestEigenValue(numIterations);

		// Check if vector as any value > or < 1 (float shows errors when rounding), even with normalization!
		if (FMath::Abs(normal.X) <= 1.0f && FMath::Abs(normal.Y) <= 1.0f && FMath::Abs(normal.Z) <= 1.0f) {
			end = center + normal;

			// CORRECT NORMAL ORIENTATION
			float centroid_to_center = FVector::DistSquared(center, FVector(0.0f));
			float end_to_center = FVector::DistSquared(end, FVector(0.0f));

			if (end_to_center < centroid_to_center)
				normal = -normal;
		}
		else {
			normal = FVector(0.0f);
		}
	}

	res = IPlane(position, center, normal);
	return res;

}
