import sys
import os
path = sys.argv[1]
os.chdir(path)
import math
sys.path.insert(0, '../../../')
sys.stderr = open('null', 'w')
from nnet import Datasets, Generic, DNN
import numpy as np

# environment settings
os.environ['TF_CPP_MIN_LOG_LEVEL']='3'
np.set_printoptions(threshold = np.inf)

# fix random seed for reproducibility
seed = 1
mode = sys.argv[2]
net_filename = '../Weights/LE_NET_Occupancy.iordcore'
weights_filename = '../Weights/weights_LE_NET_Occupancy.iordcore'

# generate train and test data frames
if mode == 'Simple':
	(X_train, Y_train), (X_test, Y_test), nb_classes = Datasets.read_iord_train_sets_single("../Models/Train/Simple/", 25, 25, 25)
elif mode == 'Advanced':
	(X_train, Y_train), (X_test, Y_test), nb_classes = Datasets.read_iord_train_sets_single("../Models/Train/Advanced/", 25, 25, 25)
elif mode == 'Custom':
	(X_train, Y_train), (X_test, Y_test), nb_classes = Datasets.read_iord_train_sets_single("../Models/Train/Custom/", 25, 25, 25)
else:
	print(-1)
	sys.exit()

acts = ['relu','','relu','','','relu','','softmax']
layers = ['conv','maxpool','conv','maxpool','flatten','dense','dropout','dense']
params = [[32,5,5,5],
		  [(2,2,2)],
		  [64,5,5,5],
		  [(2,2,2)],
		  [],
		  [1024,'normal'],
		  [0.2],
		  [nb_classes, 'normal']]
le_net_occupancy = DNN(1, 3, [25,25,25])
model = le_net_occupancy.create_model(layers = layers, params = params, acts = acts)
Generic.compile_model(model, loss = 'categorical_crossentropy', optimizer = 'adam')
history = Generic.fit_model(model, X_train, Y_train,
							epochs = 250,
							batch_size = 30,
							validation_data = (X_test, Y_test),
							verbose = 1)
Generic.save_model_json(model, net_filename)
Generic.save_weights_hdf5(model, weights_filename)
