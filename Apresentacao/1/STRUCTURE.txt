1 - Introducao
	1.1 - Problema
	1.2 - Solu��es Existentes
		1.2.1 - Reconhecimento 2D
		1.2.2 - Reconhecimento 3D
		1.2.3 - CONCEITO CORE - Extra��o de Caracter�sticas
	1.3 - Sensores
		1.3.1 - Leap Motion
		1.3.2 - Kinect
		1.3.3 - ...
	1.4 - Objetivo
		1.4.1 - Resolver o Problema das M�ltiplas Perspetivas
2 - Intelig�ncia e Vis�o por Computador
	2.1 - Redes Neuronais
	2.2 - Vis�o - Filtros e Perspetiva
		2.2.1 - Ru�do
		2.2.2 - Escala
		2.2.3 - Transla��o
		2.2.4 - Orienta��o
	2.3 - Modelos incompletos e imperfeitos
		2.3.1 - Reconstru��o
		2.3.2 - Processo de Correspond�ncia (Match/Fit)
3 - Representa��o dos Dados
	3.1 - Nuvens de Pontos
	3.2 - Voxels
		3.2.1 - Grelhas de Ocupa��o
	3.3 - Esferas Harm�nicas
		3.3.1 - Marching Cubes
		3.3.2 - Vetores de Normais
		3.3.3 - Vetores de Densidade
4 - Estado do Projeto
	4.1 - Plataforma de Desenvolvimento
		4.1.1 - Python
		4.1.2 - Unreal Engine (C++)
	4.2 - Processo de Gera��o dos Dados
	4.3 - Fase de Classifica��o
5 - Trabalho Futuro
	FASE I
		1 - Ambienta��o com as Ferramentas de Desenvolvimento (Unreal Engine, TensorFlow, C++, Python)
		2 - Ferramenta de Gera��o de Nuvens de Pontos (Cubo, Esfera, Cone, Pir�mide e Cilindro)
		3 - Processo de Voxeliza��o
		4 - Grelhas de Ocupa��o
		5 - 3D CNN (1 Canal de Input - Bin�rio (0 ou 1))
	FASE II
		6 - Marching Cubes
		7 - Esferas Harm�nicas (Vetores de Normais)
		8 - Densidade de Pontos (Vetores de Densidade)
		9 - 3D CNN v2 (M�ltiplos Canais de Input - Vetores (i, j, k))
		10 - 3D CNN v3 (M�ltiplas Perspetivas por utiliza��o de uma Camada de Rota��o)
		11 - Processo de Correspond�ncia (Match/Fit)
	FASE III
		12 - Desempenho e Escalabilidade (Tempo de Execu��o e Qualidade/Fidelidade do Grau de Semelhan�a)
		13 - Ferramenta de Scan (Kinect 2D + 3D)
		14 - Interface Gr�fica
6 - Refer�ncias
	[MODELS]
	1 - http://3dshapenets.cs.princeton.edu/ (Cloud point models)
	
	[FIRST]
	2 - https://repositorio-aberto.up.pt/handle/10216/68279 (Sistema de reconhecimento de objetos para demonstrador de condu��o rob�tica aut�noma)
	3 - http://www.mdpi.com/1424-8220/15/3/6360 (A Model-Based 3D Template Matching Technique for Pose Acquisition of an Uncooperative Space Object)
	4 - http://ieeexplore.ieee.org/document/6333871/ (A REAL TIME SYSTEM FOR DYNAMIC HAND GESTURE RECOGNITION WITH A DEPTH SENSOR)
	5 - http://cvrr.ucsd.edu/ece285/Spring2014/papers/Oikonomidis_BMVC2011.pdf (Efficient Model-based 3D Tracking of Hand Articulations using Kinect)
	6 - http://paginas.fe.up.pt/~ee06160/thesis/wp-content/uploads/2013/03/RelatorioPDI_MiguelCorreia_ee06160.pdf (Reconhecimento de Elementos Gestuais com Kinect)
	7 - http://ieeexplore.ieee.org/document/400574/ (Registering multiview range data to create 3D computer objects)
	8 - http://ieeexplore.ieee.org/document/1565434/ (Shape Model-Based 3D Ear Detection from Side Face Range Images)

	[LAST]
	9 - http://ieeexplore.ieee.org/document/7353481/ (VoxNet: A 3D Convolutional Neural Network for real-time object recognition)
	10 - http://www.dimatura.net/pages/3dcnn.html (VoxNet)
	11 - http://graphics.usc.edu/cgit/publications/papers/point_cloud_3dcnn.pdf (Point Cloud Labeling using 3D Convolutional Neural Network)
	12 - https://arxiv.org/abs/1505.00880 (Multi-view Convolutional Neural Networks for 3D Shape Recognition)
	13 - https://arxiv.org/abs/1502.02506 (Predicting Alzheimer's disease: a neuroimaging study with 3D convolutional neural networks)
	14 - http://www.di.ubi.pt/~lfbaa/pubs/ias-13.pdf (3D Object Recognition using Convolutional Neural Networks with Transfer Learning between Input Channels)
	15 - https://ai2-s2-pdfs.s3.amazonaws.com/3c86/dfdbdf37060d5adcff6c4d7d453ea5a8b08f.pdf (3D Convolutional Neural Networks for Human Action Recognition)
	16 - http://www.sciencedirect.com/science/article/pii/S0020025515005812 (3D object understanding with 3D Convolutional Neural Networks)
	
	[INVARIANTS]
	17 - http://lmb.informatik.uni-freiburg.de/Publications/2011/LSSR11/BMVC2011_79_final.pdf (3D Rotation-Invariant Description from Tensor Operation on Spherical HOG Field)
	18 - http://www.sciencedirect.com/science/article/pii/S1077316985710349 (Determination of the orientation of 3D objects using spherical harmonics)
	
	[SCAN]
	19 - http://shiffman.net/p5/kinect/ (Kinect and Processing Language)
	20 - http://3dscanexpert.com/ (Scanner)
